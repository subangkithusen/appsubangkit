<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangbmnModel extends Model
{
    protected $table = 'barangbmn';


    public function merk(){
        return $this->hasOne('App\MerkModel','id','merk_id');
    }

    public function ruang(){
        return $this->hasOne('App\RuangModel','id','ruang_id');
    }

    public function jenis(){
        return $this->hasOne('App\JenisbrgModel','id','jenis_id');
    }
    
}
