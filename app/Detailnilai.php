<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detailnilai extends Model
{
    protected $table = 'detail_nilai';
    protected $fillable = ['header_nilai_id','kompetensi_umum_id','nilai'];
    public $timestamps = false;
}

