<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detailnilaidua extends Model
{
    protected $table = 'detail_nilai_dua';
    protected $fillable = ['header_nilai_id','kompetensi_kinerja_id','nilai'];
    public $timestamps = false;
}
