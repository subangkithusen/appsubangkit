<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detailsurattugas extends Model
{
    //
    protected $table = 'detailsurattugas';
    protected $fillable = ['surattugas_id','pegawai_id'];
    public $timestamps = false;

    public function pegawai(){
        return $this->hasOne('App\PegawaiModel','id','pegawai_id');
    }

}
