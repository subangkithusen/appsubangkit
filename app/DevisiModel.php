<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevisiModel extends Model
{
    protected $table = 'divisi';

    public function getku(){
        return $this->hasMany('App\ModelKompetensikerja','divisi_id','id');
    }
}
