<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvenModel extends Model
{
    protected $table = 'event';
    protected $fillable = ['start', 'end', 'keterangan'];
    public $timestamps = false;
}
