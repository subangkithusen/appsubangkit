<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Headernilai extends Model
{
    protected $table = 'header_nilai';
    protected $fillable = ['login_id', 'tgl_penilaian','status','pegawai_dinilai'];
    public $timestamps = false;

    public function getuser(){
        return $this->hasMany('App\User','id','pegawai_dinilai');
    }

    public function detail_nilai(){//tabel detai ke-1
        return $this->hasMany('App\Detailnilai','header_nilai_id','id');
    }
}
