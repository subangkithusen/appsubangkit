<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BarangbmnModel as bbm;
use \Milon\Barcode\DNS2D;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class Bmncontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = bbm::OrderBy('id','DESC')->get();
        return view('layouts.bmn.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.bmn.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CetakRekapBRCD(Request $request, $id){
        // dd((Int)$id);
        $data = bbm::find((Int)$id);
        $ruangan =  $data->ruang['nama'];
        $url = url('/');
        $barcodeurl = $url.'/'.$request->path(); //barcode url
        // dd($barcodeurl);
        // dd($this->link_it($barcodeurl));
        $barcodetext = $data->namabarang.'-'.$ruangan.'-'.$data->pegawai_id.'-'.$data->no_inventarisbmn.'/'.$data->tanggal_diterima; //barcodetext
        
        $qr = $barcodetext;
        //barcode nya 
        $qr = QrCode::size(100)->generate($qr);
        $qrlink = QrCode::size(100)->generate($barcodeurl);
        return view('layouts.bmn.printbarcode',compact('qr','qrlink','data'));
        
    }

    public function link_it($text)  
    {  
         $text= preg_replace("/(^|[\n ])([\w]*?)([\w]*?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" >$3</a>", $text);  
         $text= preg_replace("/(^|[\n ])([\w]*?)((www)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"http://$3\" >$3</a>", $text);
                 $text= preg_replace("/(^|[\n ])([\w]*?)((ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"ftp://$3\" >$3</a>", $text);  
         $text= preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $text);  
         return($text);  
     }
}
