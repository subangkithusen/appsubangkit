<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelKompetensiumum as mu;
use App\ModelKompetensikerja as mk;
use App\Modelrate as rate;
use App\User;
use App\Headernilai;
use App\Detailnilai;
use App\Detailnilaidua;
use Illuminate\Support\Facades\Crypt;


class Controllerpenialaiankinerja extends Controller
{

    public function __construct()
    {
        // parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // fungsi ambil data berdasarkan pegawai
    public function getmk(){
        return \Request::get('pegawai');
    }

    public function index()
    {
        
        // dd(gethostname()); namapc
        // dd(\Request::ip());
        // dd($this->getmk());
        $datainput = $this->getinputpenilaian();
        $rate = rate::orderBy('nilai','DESC')->get();
        $pegawai = User::orderBy('nama','ASC')->get();
        $datamu = mu::orderBy('nama','ASC')->get();
        // dd($data);
        return view('layouts.penilaian.index',compact('rate','pegawai','datainput'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $countArray = count($request->inKU);
        $countkk = count($request->inKK);
       

        $now = \Carbon\carbon::now();
        $month = $now->month;
        $login_id= auth()->user()->id;
        // dd($login_id);//8
        $tgl_penilaian= $now;
        $status =1;
        $pegawai_yang_dinilai = $request->pegawai_yang_dinilai;

        // cek data
        $cekdata = Headernilai::where('pegawai_dinilai','=',$pegawai_yang_dinilai)->where('tgl_penilaian','like','%'.$month.'%')->where('login_id','=',$login_id)->first();
        // dd($cekdata);
        if($cekdata){
            return redirect()->route('penilaiankerja.index')->with('notif', 'Data sudah pernah dimasukkan di bulan '.$this->bulan($month).' ini !!!');
        }

        // jika tombol di submit tanpa memasukkan
        if(!isset($request->inKU)){
            return redirect()->back()->with('error', 'klik tombol cari terlebih dahulu untuk menilai pegawainya!');
        }else{
            if(auth()->user()->id == $pegawai_yang_dinilai){
                // return redirect()->route('penilaiankerja.index');
                dd("tidak boleh menilai dirinya sendiri");
                
            }
            // save header data
            $saveheader = Headernilai::create([
                'login_id' => $login_id,
                'tgl_penilaian' => $now,
                'status' => $status,
                'pegawai_dinilai'=>$pegawai_yang_dinilai 
            ]);

            $lastheader = $saveheader->id;
            $lastheader2 = $saveheader->id;

            // savedetail 1
           for($i=0;$i<$countArray;$i++){
                $detailnilai = Detailnilai::create([
                'header_nilai_id'=> $lastheader,
                'kompetensi_umum_id' => $request->inKU[$i],
                'nilai' => $request->ratemu[$i]]);
           }
            // save detail 2
            for($i=0;$i<$countkk;$i++){
                $detailnilai2 = Detailnilaidua::create([
                'header_nilai_id'=> $lastheader2,
                'kompetensi_kinerja_id' => $request->inKK[$i],
                'nilai' => $request->ratemk[$i]]);
            }
            // dd($detailnilai2);
            if($saveheader && $detailnilai && $detailnilai2){
                // return redirect()->back()->with('msg', 'Successfully data tersimpan !!!');
                // return redirect()->route('penilaian', ['category'=>513, 'id'=>1]);
                return redirect()->route('penilaiankerja.index')->with('msg', 'Successfully data tersimpan !!!');
            }else{
                dd("gagal input");

            }

        }

    }

    public function  simpanheader(){

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showpenilaian(Request $request){

        // show modal input
        $datainput = $this->getinputpenilaian();
        // dd($datainput);
        // end modal input

        $rate = rate::orderBy('nilai','DESC')->get();
        $pegawai = User::orderBy('nama','ASC')->get();
        $datamu = mu::orderBy('nama','ASC')->get();

         $idpegawai= $this->getmk();
        
        //encript 
        // $encrypted = Crypt::encryptString($idpegawai);
        // // dd($encrypted);
        // $decrypted = Crypt::decryptString($encrypted);
        // dd($decrypted);
        
        //end decript
        if($idpegawai == "-"){ //jika inputan -
            // return redirect()->back()->with('success', 'your message,here');
            // return redirect()->action('Controllerpenialaiankinerja@index');
            return redirect('penilaiankerja')->with('status', 'Mohon diisi nama pegawai yang dinilai!');
        }
        $idpegawai = Crypt::decryptString($idpegawai);
        $getPegawai = User::where('id','=',$idpegawai)->first();

        // dd($getPegawai);
        $butirMK = mk::where('divisi_id','=',$getPegawai->divisi_id)->get();

        if($butirMK->isEmpty()){
            dd("pegawai ini belum mempunyai divisi id ini");
        }else{
            $butirMK = mk::where('divisi_id','=',$getPegawai->divisi_id)->get();
            $btnsimpan = '<button class="button btn-sm btn-success" style="float: right">Beri Nilai</button>';
            return view('layouts.penilaian.index',compact('butirMK','rate','pegawai','datamu','getPegawai','idpegawai','btnsimpan','datainput'));
        }
        

    }
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    function tanggal_indo($tanggal){
            $bulan = array (1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                    );
            $split = explode('-', $tanggal);
            return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }

    public function bulan($bulan){
        if($bulan == 1){
            $bulan = 'Januari';
        }elseif($bulan == 2){
            $bulan = 'Februari';
        }elseif($bulan == 3){
            $bulan = 'Maret';
        }elseif($bulan == 4){
            $bulan = 'April';
        }elseif($bulan == 5){
            $bulan = 'Mei';
        }elseif($bulan == 6){
            $bulan = 'Juni';
        }elseif($bulan == 7){
            $bulan = 'Juli';
        }elseif($bulan == 8){
            $bulan = 'Agustus';
        }elseif($bulan == 9){
            $bulan = 'September';
        }elseif($bulan == 10){
            $bulan = 'Oktober';
        }elseif($bulan == 11){
            $bulan = 'November';
        }else{
            $bulan = 'Desember';
        }

        return $bulan;
    }

    public function getinputpenilaian(){ //json input penilaian user
        $id = auth()->user()->id;
        $data = Headernilai::where('login_id','=',$id)->orderBy('tgl_penilaian','DESC')->get();
        return $data;
    }

}
