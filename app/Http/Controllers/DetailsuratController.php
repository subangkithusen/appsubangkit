<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Detailsurattugas as ds;

class DetailsuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //    dd("test");
       dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json($id);
    }

    public function hapusPegawaiDL($id,$surat_id){
        // $data = $id.'-'.$surat_id;
        $hapus = ds::where('id','=',$id)->where('surattugas_id','=',$surat_id)->delete();
        if($hapus){
            $data['sukses'] = 200;
        }
        return response()->json($data);
    }

    public function tambahpertugas(Request $request){
        // dd((int)$request->pegawaidl);

        // petugas yang diinputkan sudah ada 

        $store = ds::create([
            'surattugas_id' => $request->surattugas_id,
            'pegawai_id'  => (int)$request->pegawaidl
        ]);

        if($store){
            $data['sukses'] = 200;
        }
        return response()->json($data);
    }

    public function ubahpegawaiJson($a,$b){
        $data = $a.'-'.$b;
        $dt = ds::where('id','=',$a)->where('pegawai_id','=',$b)->get();
        return response()->json($data);
    }
}
