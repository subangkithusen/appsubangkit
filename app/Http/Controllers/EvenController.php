<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EvenModel;

class EvenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = EvenModel::all();
        // dd($data);
        
        if($request->ajax()) {
       
            $data = EvenModel::whereDate('start', '>=', $request->start)
                      ->whereDate('end',   '<=', $request->end)
                      ->get(['id', 'keterangan', 'start', 'end']);
 
            return response()->json($data);
       }
 
       return view('testpage.fullcalender',compact('data'));
        
    }

    public function ajax(Request $request){
        switch ($request->type) {
            case 'add':
               $event = EvenModel::create([
                   'keterangan' => $request->title,
                   'start' => $request->start,
                   'end' => $request->end,
               ]);
  
               return response()->json($event);
              break;
   
            case 'update':
               $event = EvenModel::find($request->id)->update([
                   'keterangan' => $request->title,
                   'start' => $request->start,
                   'end' => $request->end,
               ]);
  
               return response()->json($event);
              break;
   
            case 'delete':
               $event = EvenModel::find($request->id)->delete();
   
               return response()->json($event);
              break;
              
            default:
              # code...
              break;
         }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
