<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Headernilai;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd("test");

        // dd(auth()->user()->id);
        $data = Headernilai::where('login_id','=',auth()->user()->id)->orderBy('tgl_penilaian','DESC')->get();
        // dd($data[0]->getuser);
        return view('home',compact('data'));
    }
}
