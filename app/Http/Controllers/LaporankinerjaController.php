<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ModelKompetensiumum as mu;
use App\ModelKompetensikerja as mk;
use App\Headernilai;
use DB;
class LaporankinerjaController extends Controller
{
    //auth
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getQueryUmum($login_id){
       // $select = DB::select("SELECT * FROM header_nilai h INNER JOIN detail_nilai dn ON h.id = dn.header_nilai_id INNER JOIN kompetensi_umum ku ON ku.id = dn.kompetensi_umum_id WHERE h.pegawai_dinilai = 100");
        $getQueryumum = DB::select('
        SELECT  dt.kompetensi_umum_id,sum(dt.nilai) as subtotal
        from(
            SELECT
            dn.kompetensi_umum_id,
            dn.nilai,
            h.pegawai_dinilai
        FROM
            detail_nilai AS dn
        INNER JOIN header_nilai AS h
        ON
            dn.header_nilai_id = h.id
        WHERE
            h.pegawai_dinilai = '.auth()->user()->id.'
        ) as dt GROUP BY dt.kompetensi_umum_id
        ');
        return $getQueryumum;

    }

    public function getQueryKinerja($login_id){
        $getQueryKinerja = DB::select('SELECT
        dt.kompetensi_kinerja_id,
        SUM(dt.nilai) AS subtotal
        FROM
            (
            SELECT
                dn.kompetensi_kinerja_id,
                dn.nilai,
                h.pegawai_dinilai
            FROM
                detail_nilai_dua AS dn
            INNER JOIN header_nilai AS h
            ON
                dn.header_nilai_id = h.id
            WHERE
                h.pegawai_dinilai = '.auth()->user()->id.'
        ) AS dt
        GROUP BY
            dt.kompetensi_kinerja_id');

        return $getQueryKinerja;
    }

    public function sumUmum(){ 
        /*menghitung total point dari semua terus dibagi total headernya pada kompetisi umum */
        $count = 0;
        $jumlahpenilai = Headernilai::where('pegawai_dinilai','=',auth()->user()->id)->count(); //header->3
        $jumlahku = mu::all()->count();
        $data = $this->getQueryUmum(auth()->user()->id);
        // total semua point umum
        $map = collect($data)->map(function($item,$value)use($count){
            $count = (int)$item->subtotal + $count;
            return $count;
        })->sum();
        $avg = $map / ($jumlahpenilai*$jumlahku);
        return $avg;
    }
    public function sumKinerja(){ //menghitung berdasarkan kinerja 
        $count = 0 ;
        $user = User::where('id','=',auth()->user()->id)->first();
        // dd($user->getdivisi->id);
        $jumlahpenilai = Headernilai::where('pegawai_dinilai','=',auth()->user()->id)->count();
        $jumlahku = mk::where('divisi_id','=',$user->getdivisi->id)->count();
        $data = $this->getQueryKinerja(auth()->user()->id);
        $map = collect($data)->map(function($item,$value)use($count){
            $count = (int)$item->subtotal + $count;
            return $count;
        })->sum();//1680
        $avg = $map / ($jumlahpenilai*$jumlahku);
        return $avg;
    }
    public function grade(){
        $totalavg = ($this->sumKinerja() + $this->sumUmum()) / 2;
        // $totalavg = 71;
        $grade = '';
        if($totalavg >= 90 && $totalavg <= 100){
            $grade = 'Sangat Baik';
        }elseif($totalavg <= 90 && $totalavg > 80){
            $grade = "Baik";
        }elseif($totalavg <= 80 && $totalavg > 70){
            $grade = "Cukup";
        }elseif($total <= 70 && $totalavg > 60){
            $grade = "Tidak Baik";
        }elseif($totalavg <=60 && $totalavg >50){
            $grade = "Sangat Tidak Baik";
        }else{
            $grade = "-";
        }
        return $grade;
    } 
    public function index(){
        $komp_umum = array();
        //ambil data kinerja umum
        $kinerja = $this->getQueryUmum(auth()->user()->id);
        //ambil data kinerja berdasarkan divisi
        $kinerja = $this->getQueryKinerja(auth()->user()->id);
        $users = User::orderBy('nama','ASC')->get();
        $jumlahpenilai = Headernilai::where('pegawai_dinilai','=',auth()->user()->id)->count();
        return view('layouts.penilaian.laporan.index',compact('users','jumlahpenilai'));
    }
    public function cetak(){
        //grade
        $grade = $this->grade();
        $pegawai = User::where('id','=',auth()->user()->id)->first();
        // total penilai
        $jumlahpenilai = Headernilai::where('pegawai_dinilai','=',auth()->user()->id)->count();
        //ambil data kinerja umum
        $umum = $this->getQueryUmum(auth()->user()->id);
        $count = 0;
        $mapumum = collect($umum)->map(function($item,$value)use($count){
            $item->nama = mu::where('id','=',$item->kompetensi_umum_id)->first();
            return $item;
        });
        //ambil data kinerja berdasarkan divisi
        $kinerja = $this->getQueryKinerja(auth()->user()->id);
        $mapkinerja = collect($kinerja)->map(function($item,$value){
            $item->nama = mk::where('id','=',$item->kompetensi_kinerja_id)->first();
            return $item;
        });
        return view('layouts.penilaian.laporan.cetak',compact('mapumum','mapkinerja','jumlahpenilai','pegawai','grade'));
    }
    public function dinilai(){
        // data 
        $data = Headernilai::where('pegawai_dinilai','=',auth()->user()->id)->get();
        $map = $data->map(function($i,$v){ //proses penjumlahan
            return $i;
        });
        return $map;
    }

    public function nilailama($id){ //nilai tahun lalu

    }


    public function getdatainput(){
        $data = "data";
        return json()->response($data);
    }
}