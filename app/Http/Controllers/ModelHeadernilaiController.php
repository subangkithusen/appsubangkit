<?php

namespace App\Http\Controllers;

use App\ModelHeadernilai;
use Illuminate\Http\Request;

class ModelHeadernilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelHeadernilai  $modelHeadernilai
     * @return \Illuminate\Http\Response
     */
    public function show(ModelHeadernilai $modelHeadernilai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelHeadernilai  $modelHeadernilai
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelHeadernilai $modelHeadernilai)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelHeadernilai  $modelHeadernilai
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelHeadernilai $modelHeadernilai)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelHeadernilai  $modelHeadernilai
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelHeadernilai $modelHeadernilai)
    {
        //
    }
}
