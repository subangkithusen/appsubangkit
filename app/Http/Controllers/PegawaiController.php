<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PegawaiModel;
use App\GolonganModel as gm;
use App\JabatanModel as jm;
use App\DevisiModel as dm;
use \Milon\Barcode\DNS2D;


class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //Post::orderBy('id', 'DESC')->get();
    //   $qr = DNS2D::getBarcodeHTML('4445645656', 'QRCODE');
      $data = Pegawaimodel::orderBy('nama','ASC')->get();
     
    //   dd($data[0]->jabatan);
      return view('layouts.pegawai.index',compact('data')); 
    }

    /**
     * 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gm = gm::orderBy('golongan','ASC')->get();
        $jm = jm::orderBy('namajabatan','ASC')->get();
        $dm = dm::orderBy('namadivisi','ASC')->get();
        // dd($dm);
        return view('layouts.pegawai.create',compact('gm','jm','dm')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
