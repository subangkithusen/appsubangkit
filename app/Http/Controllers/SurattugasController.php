<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PegawaiModel as pm;
use App\SurattugasModel as sm;
use App\Detailsurattugas as dst;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;
// use App\Http\Controllers\Storage;

class SurattugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $datasurat = sm::orderBy('id','DESC')->get();
        return view('layouts.surattugas.index',compact('datasurat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pegawais = pm::orderBy('nama','ASC')->get();
        // dd($pegawasis);
        return view('layouts.surattugas.create',compact('pegawais'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*jika pegawai lebih dari 8 orang maka masuk ke cetak lampiran */ 
        $nomorconc = $request->pertama.'.'.$request->kedua.'.'.$request->ketiga.'/'.$request->keempat;
        // dd($nomorconc);

        // dd($request->all());
        // dd($request->petugas);
        // $dataarry = count($request->petugas);
        // dd($dataarry);
        if(empty($request->petugas)){
            dd("Petugas masih kosong tolong diisi");
        }else{
            // jika sukses 

            //pengecekkan jika petugas lebih dari 8 maka dibuatkan lampiran 
            $datasurat = sm::create([
                'nomorsurat' =>$request->nomer,
                'aliassurat' =>$nomorconc,
                'acara' =>$request->perihal,
                'tanggal_mulai' =>Carbon::createFromFormat('d/m/Y', $request->tglmulai)->format('Y-m-d'),
                'tanggal_selesai' =>Carbon::createFromFormat('d/m/Y', $request->tglselesai)->format('Y-m-d'),
                'koordinator_id'=>$request->koordinator,
                'konten' =>$request->editor1,
                'status_lampiran' =>0,
                'sign_id'=>$request->persetujuan,
                'created_at' => Carbon::now()
            ]);

            // insert tabel detail
            foreach($request->petugas as $pt){
                $dst = dst::create([
                    'surattugas_id'=> $datasurat->id,
                    'pegawai_id'=> $pt
                ]);

            }
            

            dd($dst);


            dd($datasurat);
        }
      

        


        // preview dulu
        $qr = QrCode::size(50)->generate('subangkit');
        // $qr = 'data';
        // $datasurat = $request->all();

        // $pdf = PDF::loadView('layouts.surattugas.print.preview',['datasurat'=>$datasurat,'qr'=>$qr]);
        // return $pdf->stream(); //browser
        //return $pdf->download('laporan-pegawai.pdf'); // download
        
        
    
        // dd($request->all());
        $datasurat = $request->all();
        
        // dd($datasurat['editor1']);
        return view('layouts.surattugas.print.preview',compact('datasurat','qr'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    //    nnomor di pecah 
        $surat = sm::find($id);
        // dd($surat);
        // dd($surat->detailsurat);


    //   konten
       $pegawais = pm::orderBy('nama','ASC')->get();
       return view('layouts.surattugas.show',compact('pegawais','surat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function jsonpegawai($id){//json pilih
        $pegawai = pm::find($id);
        $data['id'] = $pegawai->id;
        $data['nik'] = $pegawai->nik;
        $data['nama'] = $pegawai->nama;
        $data['pangkat'] = $pegawai->golongan->namapangkat."/".$pegawai->golongan->golongan;
        $data['jabatan'] = $pegawai->jabatan->namajabatan;
        // dd($data);
        return response()->json($data);
    }


    public function cetakpdf($id){
        // dd($id);
        $surat = sm::find($id);
        $qr = QrCode::size(60)->generate('subangkit');
        // $qr=QrCode::size(60)->format('png')->mergeString(Storage::get('public/logo/logo.png'))->generate('subangkit');
        
        // $qr=QrCode::size(60)->format('png')->mergeString())->generate('subangkit');
        // dd($qr);
        //asset('storage/uploads/avatar/default.jpg')
        // dd($surat);
        return view('layouts.surattugas.download.index',compact('surat','qr'));

        // $pdf = PDF::loadView('layouts.surattugas.download.index',['surat'=>$surat,'qr'=>$qr]);
        // $pdf = $pdf->setPaper('legal');
        // return $pdf->stream(); //browser
        // return $pdf->download('laporan-pegawai.pdf'); // download
        // dd($surat);
    }

    public function berinomor(Request $request){
        // $wordCount = Wordlist::where('id', '<=', $correctedComparisons)->count();
        $cek = sm::where('nomorsurat','=',$request->nomor)->count();
        if($cek == 1){
            // dta ada
            $data = array(
                'status_code' => 1
            );
        }else{
            // update nomer surat yang ada di tabel surattugas 
            $surat = sm::find($request->idsurat);
            $surat->nomorsurat = $request->nomor;
            $surat->save();
            $data = array(
                'status_code' => 0
            );
        }
        return response()->json($data);

    }


}
