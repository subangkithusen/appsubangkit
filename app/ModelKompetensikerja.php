<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelKompetensikerja extends Model
{
    protected $table = 'kompetensi_kinerja';
    protected $fillable = ['nama', 'keterangan','status','divisi_id'];
    public $timestamps = false;


    public function divisi(){
        return $this->hasOne('App\DevisiModel','id','divisi_id');
	} 
}




