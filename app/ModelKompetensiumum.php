<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelKompetensiumum extends Model
{
    protected $table = 'kompetensi_umum';
    protected $fillable = ['nama', 'keterangan','status'];
    public $timestamps = false;
}
