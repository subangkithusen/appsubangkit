<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelrate extends Model
{
    protected $table = 'rate';
    protected $fillable = ['nama', 'nilai','status'];
    public $timestamps = false;
}
