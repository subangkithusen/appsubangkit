<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PegawaiModel extends Model
{
    //
    protected $table = 'pegawai';
    protected $fillable = [];
    public $timestamps = false;

    //divisi
    public function devisi(){
        return $this->hasOne('App\DevisiModel','id','divisi_id');
    }

    //golongan
    public function golongan(){
        return $this->hasOne('App\GolonganModel','id','golongan_id');
    } 

    //jabatan 
    public function jabatan(){
        return $this->hasOne('App\JabatanModel','id','jabatan_id');
    }



}
