<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurattugasModel extends Model
{
    protected $table = 'surattugas';
                // 'konten' =>,
    protected $fillable = ['nomorsurat','aliassurat','acara','tanggal_mulai','tanggal_selesai','koordinator_id','konten','status','status_lampiran','sign_id'];

    public function koordinator(){
        return $this->hasOne('App\PegawaiModel','id','koordinator_id');
        
    }

    public function detailsurat(){
        return $this->hasMany('App\Detailsurattugas','surattugas_id','id');
    }
}

