<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script> -->
    {{-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> --}}
    <!-- datepicker -->
    {{-- animasi --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/r121/three.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vanta@latest/dist/vanta.net.min.js"></script>


    <!-- select 2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    
    

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('css')
    <style type="text/css">
        .select2{
            //*width:auto !important;*/
        }
        ul.menusjp{
            
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            padding-top: 12px;
            /*background-color: #333333;*/
            }

            li .menutop{
            float: left;
            }

            li a{
            display: block;
            color: black;
            text-align: center;
            /*padding: 10px;*/
            text-decoration: none;
            padding-top: 2px;
            padding-bottom: 5px;
            padding-right: 5px;
            margin-right: 5px;
            }

            li a:hover .menutop{
            background-color: #b2bec3;
            text-decoration: none;
            color: black;
            }

            /*pagination datatable*/
            

            /*end pagination*/

            /*mini tabel row*/
            .table {
                font-size: 12px;
            }

            .table tr,.table td {
                height: 12px;
                /*text-align: center*/
            }

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
            {
                padding:2px; 
            }
    </style>
</head>
<body style="background-image:url({{ url('gambarlogo/puzzle.svg') }});background-size: 800px 600px;">
<div class="container" style="margin-top:50px;padding-top:20px;">
    <div class="row" style="opacity: 0.96;">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#305399;color:white"><b>Simple Apps</b></div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus autocomplete="off">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                ** Jika username/email belum terdaftar, silahkan hubungi mas mas yang ada di tatausaha
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Masuk
                                </button>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-6" style="opacity: 0.95;">
        {{-- jumbo --}}
        <div class="jumbotron">
            <h3 class="display-4">Motivasi</h3>
            <p class="lead">" Hiduplah seakan-akan kamu akan mati besok. Belajarlah seakan-akan kamu akan hidup selamanya "</p>
            <hr class="my-4">
            <p>Mahatma Gandhi</p>
            
        </div>
        {{-- end --}}
        </div>
    </div>
</div>
<script>
        VANTA.NET({
        el: "#your-element-selector",
        mouseControls: true,
        touchControls: true,
        gyroControls: false,
        minHeight: 200.00,
        minWidth: 200.00,
        scale: 1.00,
        scaleMobile: 1.00,
        color: 0xffffff,
        backgroundColor: 0x20076e
        })
        </script>
</body>
</html>

