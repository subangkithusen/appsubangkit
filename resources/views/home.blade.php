@extends('layouts.app')
@section('content')
<div class="container">
    {{-- profil --}}
    {{-- <div class="row" style="margin:0px !important;padding:0px !important">
        <div class="col-md-12" style="margin:0px !important;padding:0px !important">
            
        </div>
    </div> --}}
    {{-- end profil --}}

    <div class="row" style="">
        <div class="col-md-8">
            <div class="panel panel-default" >
                <div class="panel-heading" style="background-color:#305399;color:white">Pegawai yang sudah dinilai :</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{-- table yang sudah diisi --}}
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>NAMA PEGAWAI</th>
                                <th>DIVISI</th>
                                <th>TANGGAL</th>
                                <th></th>   
                            </tr>
                        </thead>
                            <tbody>
                            <?php $i=1?>
                            @foreach($data as $key => $value)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $value->getuser[0]->nama}}</td>
                                <td>{{ $value->getuser[0]->getdivisi->namadivisi }}</td>
                                <td>{{ $value->tgl_penilaian }}</td>
                                <td><button class="btn btn-xs btn-warning">hapus</button></td>
                                
                            </tr>      
                            @endforeach
                            

                            </tbody>
                        </table>

                    {{-- end table sudah diisi penilaian --}}

                    {{-- progressbar --}}
                    <label>yang sudah dinilai : </label>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">
                            0%
                        </div>
                    </div>

                    {{-- end progressbar --}}


                    
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#305399;color:white">Pemberitahuan</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- PLH --}}
                    <div class="row">
                        <div class="col-md-12">
                            PLH Hari ini : <br>
                        </div>
                        
                    </div>

                    {{-- sisacuti --}}
                    <div class="row">
                        <div class="col-md-12">
                            Sisa Cuti Anda :
                        </div>
                        
                    </div>
                    {{-- pengajuan cuti --}}
                    <div class="row">
                        <div class="col-md-12">
                            <a href="#" class="btn btn-sm btn-default" style="float:right;background-color:#305399;color:white">Pengajuan Cuti <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>


    </div>


    {{-- pengumuman footer --}}
    <div class="row">
        <div class="col-md-12">
                <div class="panel panel-default" >
                    <div class="panel-heading" style="background-color:#305399;color:white">Info tambahan :</div>
                        <div class="panel-body">
                        Belum ada info terbaru 
                        </div>
                </div>
                
        </div>
    </div>

</div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            //$('#example').DataTable();

            $('#example').DataTable( {
                responsive: true,
                "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]]
            } );
        } );
    </script>
@endsection
