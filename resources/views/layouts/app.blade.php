<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script> -->
    {{-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> --}}
    <!-- datepicker -->


    <!-- select 2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    
    

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('css')
    <style type="text/css">
        .select2{
            //*width:auto !important;*/
        }
        ul.menusjp{
            
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            padding-top: 12px;
            /*background-color: #333333;*/
            }

            li .menutop{
            float: left;
            }

            li a{
            display: block;
            color: black;
            text-align: center;
            /*padding: 10px;*/
            text-decoration: none;
            padding-top: 2px;
            padding-bottom: 5px;
            padding-right: 5px;
            margin-right: 5px;
            }

            li a:hover .menutop{
            background-color: #b2bec3;
            text-decoration: none;
            color: black;
            }

            /*pagination datatable*/
            

            /*end pagination*/

            /*mini tabel row*/
            .table {
                font-size: 12px;
            }

            .table tr,.table td {
                height: 12px;
                /*text-align: center*/
            }

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
            {
                padding:2px; 
            }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom:5px !important;background-color:#305399;color:white">
            <div class="container">
                <div class="navbar-header" >

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}/home">
                        <!-- {{ config('app.name', 'Laravel') }} -->
                        <b  style="color:white"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</b>
                    </a>
                    {{-- <a class="navbar-brand" style="margin-top:0px;font-size:12px !important;color:#b2bec3"><b><i class="glyphicon glyphicon-user" style="color:#dfe6e9"></i> {{ title_case(Auth::user()->nama) }}</></a> --}}
                    {{-- <a class="navbar-brand" href="{{url('/penilaiankerja')}}">Penilaian</a> --}}

                    
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <!--<li><a href="{{ route('login') }}">Login</a></li>-->
                            <!--<li><a href="{{ route('register') }}">Register</a></li>-->
                        @else

                        <li>
                        <!-- menu ke 2  -->
                            <!-- <div class=""> -->
                                <ul class="menusjp" id="menutop" >
                                    <li class="menutop"> <a href="#" class="menutop" style="color:white">{{ Auth::user()->nama }}</a> </li>
                                    <li class="menutop"> <a href="{{url('/penilaiankerja')}}" class="menutop" style="color:white">+ Halaman Penilaian Pegawai</a></li>
                                    <li class="menutop"> <a href="{{url('/laporan')}}" class="menutop" style="color:white">+ Hasil Penilaian/Rate</a></li>
                                    <li class="menutop"><a href="{{ route('logout') }}"  style="text-decoration:none !important;background-color:#7db5db;padding-left:5px;color:white;font-family:Roboto;border-radius:10px 2px;"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a></li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    
                                    
                                </ul>
                                
                            <!-- </div> -->
                        <!-- end menu ke 2 -->
                        </li>


                            {{-- <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->nama }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li> --}}

                            
                        @endif
                    </ul>
                </div>

                
            </div>
        </nav>

        @yield('content')
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
 
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script> -->
    <script type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <!-- tambahan -->
        <!-- end select 2 -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
    <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

    <!-- datat -->
    @yield('js')
    
   
   
</body>
</html>
