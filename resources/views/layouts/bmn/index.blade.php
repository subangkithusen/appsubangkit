@extends('layouts.app')

@section('content')
<div class="container">

<!-- cetak all barcode fileter -->
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <p>Filter cetak </p>

      </div>

      <div class="panel-body">
        <form action="#">
        <div class="form-group">
          <label for="sel1">Pilih Ruangan:</label>
          <select class="form-control" id="sel1">
            <option>All</option>
            <option>TU</option>
          </select>
          <button class="btn btn-md btn-primary" style="margin-top:5px"> CARI </button>
        </div>
          
        </form>

      </div>

    </div>

  </div>

</div>
<!-- end cetak barcode -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading"><a href="{{route('bmn.create')}}"><span class="btn btn-sm btn-success">+ Buat Baru</span></a></div>

                <div class="panel-body">
                    <p>DATA BMN BPFK Surabaya</p>
                  

                    <table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">NOMER INVENTARIS</th>
      <th scope="col">NAMA BARANG</th>
      <th scope="col">MERK</th>
      <th scope="col">RUANG</th>
      <th scope="col">JENIS </th>
      <th scope="col">TANGGAL TERIMA</th>
      <!-- <th scope="col">SATUAN</th> -->
      <!-- <th scope="col">PEGAWAI</th> -->
      <th scope="col">DETAIL BARANG</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
   
  @foreach($data as $dt)
    <tr>
      <td>{{$dt->no_inventarisbmn}}</td>
      <td>{{$dt->namabarang}}</td>
      <td>{{$dt->merk['namamerk']}}</td>
      <td>{{$dt->ruang['nama']}}</td>
      <td>{{$dt->jenis['namajenis']}}</td>
      <td>{{$dt->tanggal_diterima}}</td>
      <td>{{$dt->detailbarang}}</td>
      <td><a href="{{URL::to('/cetaksinglebarcode')}}/{{$dt->id}}" class="btn btn-sm btn-info" target="blank">BRCD</a> <a href="{{URL::to('/')}}" class="btn btn-sm btn-warning">info</a></td>
    </tr>
    @endforeach
  
    
  </tbody>
</table>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
