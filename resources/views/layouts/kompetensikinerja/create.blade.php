@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading"><a href="{{route('kompetensiumum.create')}}"><span class="btn btn-sm btn-success">+ Buat Baru</span></a></div>

                <div class="panel-body">
                    <p>Data Kompetensi Kinerja berdasarkan Divisi</p>
                    <!-- info -->
                    @if(isset($info))
                    <div class="alert alert-success" role="alert">{{$info}}</div>
                    @else
                    
                    @endif

                    
                    <form method="POST" action="{{route('kompetensiumum.store')}}">
                      {{csrf_field()}}
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="masukkan nama kompetensi" autocomplete="off" name="nama" required >
                        
                        
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Keterangan</label>
                        <input type="text" class="form-control" id="" placeholder="masukkan keterangan" name="keterangan" required autocomplete="off">
                       
                      </div>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
