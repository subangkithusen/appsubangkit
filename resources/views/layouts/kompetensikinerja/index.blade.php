@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading"><a href="{{route('kompetensikinerja.create')}}"><span class="btn btn-sm btn-success">+ Buat Baru</span></a></div>

                <div class="panel-body">
                    <p>Data Kompetensi Kinerja</p>
                  

                    <table class="table table-sm">
                      <thead>
                        <tr>
                          <th scope="col">No</th>
                          <th scope="col">Nama</th>
                          <th scope="col">Divisi</th>
                          <th scope="col">Keterangan</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $i =1;
                        ?>
                       <!-- data -->
                       @foreach($data as $dt)
                       <tr>
                          <td>{{$i++}}</td>
                          <td>{{$dt->nama}}</td>
                          <td>{{$dt->divisi->namadivisi}}</td>
                          <td>{{$dt->keterangan}}</td>
                          <td><a href="#" class="btn btn-default btn-sm">Ubah</a></td>
                        </tr>

                       @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
