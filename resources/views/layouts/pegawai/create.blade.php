@extends('layouts.app') @section('content') <div class="container">
@push('css')
<!-- script untuk css -->
<style type="text/css">
  .select2 {
  width:100%!important;
  }


</style>
@endpush
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Form Master Pegawai<a href="{{URL::to('/sjp')}}/create" class="btn btn-sm btn-info" style="float: right">Refresh</a></div>
        <div class="panel-body">
            <!-- form -->
            <div class="row">
                <div class="col-md-6">
                  <!-- konten kiri -->
                  <div class="form-group">
                    <label for="email">Nama:</label>
                    <input type="email" class="form-control" onfocus="this.value=''" autocomplete="off" id="email">
                  </div>

                  <div class="form-group">
                    <label for="email">NIP:</label>
                    <input type="email" class="form-control" id="email" autocomplete="off">
                  </div>


                  <div class="form-group">
                    <label for="email">NIK:</label>
                    <input type="email" class="form-control" id="email" placeholder="kosongi jika tidak punya" autocomplete="off">
                  </div>


                  <div class="form-group">
                    <label for="email">TEMPAT LAHIR:</label>
                    <input type="email" class="form-control" id="email" autocomplete="off">
                  </div>

                  <div class="form-group">
                    <label for="email">TANGGAL LAHIR:</label>
                    <input type="email" onfocus="this.value=''" class="form-control" id="email" autocomplete="off">
                  </div>

                  <div class="form-group">
                    <label for="email">PENDIDIKAN:</label>
                    <input type="email" class="form-control" id="email" autocomplete="off">
                  </div>


                  <div class="form-group">
                    <label for="email">NO HP:</label>
                    <input type="email" class="form-control" id="email"  placeholder="kosongi jika tidak punya" autocomplete="off">
                  </div>


                  <div class="form-group">
                    <label for="email">EMAIL:</label>
                    <input type="email" class="form-control" id="email"  placeholder="kosongi jika tidak punya" autocomplete="off">
                  </div>




                  <!-- end kiri -->
                </div>

                <div class="col-md-6">
                   <!-- konten kanan -->

                   <div class="form-group">
                    <label for="email">TMT CPNS:</label>
                    <input type="email" class="form-control" id="email"  placeholder="kosongi jika tidak punya" autocomplete="off">
                  </div>

                  <div class="form-group">
                    <label for="email">TMT PANGKAT :</label>
                    <input type="email" class="form-control" id="email"  placeholder="kosongi jika tidak punya" autocomplete="off">
                  </div>

                  <div class="form-group">
                    <label for="divisi">GOLONGAN</label>
                    <select id="golongan" class="form-control" name="golongan" autocomplete="off"> 
                      @foreach($gm as $g)
                      <option value="{{$g->id}}" width="100%">{{$g->golongan}} / {{$g->namapangkat}}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="divisi">JABATAN</label>
                    <select id="jabatan" class="form-control" name="jabatan" autocomplete="off"> 
                      @foreach($jm as $j)
                      <option value="{{$j->id}}">{{$j->namajabatan}}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="divisi">DIVISI</label>
                    <select id="divisi" class="form-control" name="divisi" autocomplete="off"> 
                      
                      @foreach($dm as $d)
                      <option value="{{$d->id}}">{{$d->namadivisi}}</option>
                      @endforeach
                    </select>
                  </div>

                  
                   <!-- en kanan -->

                </div>

            </div>

            <div class="row">
               
                    <div class="col-md-4">
                        <button class="btn btn-md btn-success">SIMPAN</button>
                        <button class="btn btn-md btn-warning">BERSIHKAN</button>
                    </div>

                    

                

            </div>


            
            <!-- end form -->
        </div>
      </div>
    </div>
  </div>
</div> @endsection

@section('js')
<script type="text/javascript">
  
  $(document).ready(function(){
    
    $('#golongan').select2();

     $('#jabatan').select2();
     $('#divisi').select2();


    $('#add').click(function(event){
    var tambahkotak = $('.isi');
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;

    console.log(selectedValue);
    // alert(selectedValue)

    event.preventDefault(); 
    var tabel ='';
    $('<div><label><input name="pegawai[]" value="'+selectedValue+'"/></label><button id="remove" class="btn btn-sm btn-danger" style="padding-bottom:3px;margin-bottom:3px">X</button></div>').appendTo(tambahkotak);
  });



  //   $('<div id="box"><textarea id="name" class="name"/></textarea><button id="remove">Hapus</button></div>').appendTo(tambahkotak);
  // });
  
  $('body').on('click','#remove',function(){  
    $(this).parent('div').remove(); 
  });   


  });


</script>

@endsection