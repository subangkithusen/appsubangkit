@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading"><a href="{{route('pegawai.create')}}"><span class="btn btn-sm btn-success">+ Buat Baru</span></a></div>

                <div class="panel-body">
                    <p>Data Pegawai</p>
                  

                    <table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">NIP</th>
      <th scope="col">Tempat/ Tgl Lahir</th>
      <th scope="col">Golongan</th>
      <th scope="col">Jabatan</th>
      <th scope="col">Divisi</th>
      <th scope="col">No Hp</th>
      <th scope="col">Email</th>

    </tr>
  </thead>
  <tbody>
    <?php
    $i =1;
    ?>
    @foreach($data as $dt)
    <tr>
      <td>{{$i++}}</td>
      <td>{{$dt->nama}}</td>
      <td>{{$dt->nip}}</td>
      <td>{{$dt->tempat_lahir}} / {{$dt->tanggal_lahir}}</td>
      <td>{{$dt->golongan['golongan']}}</td>
      <td>{{$dt->jabatan['namajabatan']}}</td>
      <td>{{$dt->devisi['namadivisi']}}</td>
      <td>{{$dt->nohp}}</td>
      <td>{{$dt->email}}</td>
    </tr>
    @endforeach
    
  </tbody>
</table>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
