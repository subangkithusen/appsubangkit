@extends('layouts.app')

@section('content')
<div class="container">
{{-- modal --}}
<div class="row">
	<div class="col-md-12">
		{{-- isimodal --}}
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
				<div class="modal-header" style="background-color:#305399;color:white">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Data Pegawai yang sudah dinilai</h4>
				</div>
				<div class="modal-body">
					{{-- tabel --}}
					<table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>NAMA PEGAWAI</th>
                                <th>DIVISI</th>
                                <th>TANGGAL</th>
                                   
                            </tr>
                        </thead>
                            <tbody>
                            <?php $i=1?>
                            @foreach($datainput as $key => $value)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $value->getuser[0]->nama}}</td>
                                <td>{{ $value->getuser[0]->getdivisi->namadivisi }}</td>
                                <td>{{ $value->tgl_penilaian }}</td>
                               
                                
                            </tr>      
                            @endforeach
                            

                            </tbody>
                        </table>
					{{-- end tabel --}}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				</div>

  </div>
</div>

	</div>
</div>
{{-- end modal --}}
	<div class="row" >
		<div class="col-md-12">
			 <div class="panel panel-default">
			 	<div class="panel-heading" style="background-color:#305399;color:white">Kriteria Nilai</div>

			 	<div class="panel-body">
			 		<span class="label label-primary">Sangat Baik - 100 POIN</span>
			 		<span class="label label-success">Baik - 90 POIN</span>
			 		<span class="label label-info">Cukup - 80 POIN</span>
			 		<span class="label label-warning">Tidak Baik - 70 POIN</span>
			 		<span class="label label-danger">Sangat Tidak Baik - 60 POIN</span>
			 	</div>
			 </div>
			
		</div>
	</div>

	<!-- pengisian / form -->
	<div class="row">
		<div class="col-md-12">
			 <div class="panel panel-default">
			 	<div class="panel-heading" style="background-color:#305399;color:white">Form Pemberian Nilai / Rate <button type="button" class="btn btn-default btn-xs" style="color:white;background-color:#305399;float:right" data-toggle="modal" data-target="#myModal">Lihat</button></div>
			 	<div class="panel-body">
			 		<!-- row body-->

			 		<div class="row">
			 			<div class="col-md-8">
			 					<form method="GET" action="{{url('/showpenilaian')}}">
			 						<label>	** Pegawai yang dinilai kinerjanya : </label>
			 						<select class="js-example-basic-single" name="pegawai" style="font-size: 12px">
									 		<option value="-">-</option>
			 							@foreach($pegawai as $pg)
	                                  		<option value="{{ Illuminate\Support\Facades\Crypt::encryptString($pg->id)}}">{{ title_case($pg->nama) }}</option>
	                                  	@endforeach
	                                 
	                                </select>
	                                <button class="btn btn-sm btn-success">cari</button>
			 					</form>
								
			 			</div>

						 <div class="col-md-4">
						 
								@if (session('status'))
									<div class="alert alert-warning">
										{{ session('status') }}
									</div>
								@endif

								@if (session('notif'))
									<div class="alert alert-warning">
										{{ session('notif') }}
									</div>
								@endif

								@if (session('msg'))
									<div class="alert alert-success">
										{{ session('msg') }}
									</div>
								@endif

								@if(isset($getPegawai))
									<div class="alert alert-info" style="padding:2px;float:left;margin-top:5px"><span class="glyphicon glyphicon-user"></span>
										<b>{{ $getPegawai->nama }}</b>
									</div>
								@else
								@endif
									
									
			 			</div>
						 
			 		</div>

			 		<!-- kompetensi umum -->
			 		<div class="row">
			 			<form method="POST" action="{{route('penilaiankerja.store')}}">
			 			<div class="col-md-12">
			 					<!-- tabel k.umum -->

			 					@if(isset($getPegawai))
			 						<input type="hidden" name="pegawai_yang_dinilai" value="{{$getPegawai->id}}">
			 					@else
			 						
			 					@endif
			 					{{csrf_field()}}
			 					
			 					<span class="label label-default">Kompetensi Umum</span>
			 					<table  class="table table-striped">
			 							<thead>
			 								<tr style="">
			 									<th width="50px" style="">No</th>
			 									<th width="650px" style="">Butir Penilaian</th>
			 									<th style="">Pemberian Nilai / Rate</th>
			 								</tr>
			 							</thead>
			 							<tbody style="">
			 								<?php
			 								$a=1;
			 								?>
			 								@if(isset($datamu))
			 								@foreach($datamu as $du)
			 								<tr>
			 									<td style="border: none !important;">{{$a++}}</td>
			 									<td style="border: none !important;">{{$du->nama}}<input type="hidden" value="{{$du->id}}" name="inKU[]" /></td>
			 									<td style="border: none !important;">
			 										<?php
			 										$countr = 1;
			 										?>
			 										<select name="ratemu[]">
			 											@foreach($rate as $rt)
			 											<option value="{{$rt->nilai}}">{{$rt->nama}}</option>
			 											@endforeach
			 											
			 										</select>
			 										
			 										
			 									</td>
			 									
			 								</tr>
			 								@endforeach
			 								@else
			 								<tr>
			 									<td></td>
			 									<td></td>
			 									<td></td>
			 								</tr>


			 								@endif
			 							</tbody>
			 					</table>
			 					<!-- end tabel k.umum -->
			 			</div>
			 		</div>

			 		<!-- kompetensi kinerja -->
			 		<div class="row">
			 			<div class="col-md-12">

			 				<!-- tabel kompetensi kinerja -->
			 				<span class="label label-default">Kompetensi Kinerja</span>
			 					<table  class="table table-striped">
			 							<thead>
			 								<tr>
			 									<th width="50px">No</th>
			 									<th width="650px">Butir Penilaian</th>
			 									<th>Pemberian Nilai / Rate</th>
			 									

			 								</tr>
			 							</thead>
			 							<tbody>
			 								<?php 
			 								$i=1;
			 								?>
			 								@if(isset($butirMK))
			 								@foreach($butirMK as $butir)
			 								<tr>
			 									<td style="border: none !important;">{{$i++}}</td>
			 									<td style="border: none !important;">{{$butir->nama}}<input type="hidden" value="{{$butir->id}}" name="inKK[]" /></td>
			 									<td style="border: none !important;">
			 										<select name="ratemk[]">
			 											@foreach($rate as $rt)
			 											<option value="{{$rt->nilai}}">{{$rt->nama}}</option>
			 											@endforeach
			 										</select>
			 									</td>
			 									
			 								</tr>
			 								@endforeach
			 								@else
			 								<tr>
			 									<td></td>
			 									<td></td>
			 									<td></td>
			 								</tr>

			 								@endif
			 							</tbody>
			 					</table>
			 					<!-- end tabel k.umum -->
			 				<!-- end tabel kompetensi kinerja-->
			 			
			 			</div>
			 		</div>

			 		<div class="row">
			 			<div class="col-md-12" >
						 @if(isset($btnsimpan))
						  {!! $btnsimpan !!}
						 @else
						 @endif
						
			 				

			 			</div>
			 		</div>
			 		</form>

			 		<!-- end row body -->

			 	</div>
			 </div>


			
		</div>
	</div>

	<!-- end pengisian -->
</div>
@endsection
@section('js')
<script type="text/javascript">
	  	$(document).ready(function() {
    		$('.js-example-basic-single').select2();
			$('#example').DataTable( {
                responsive: true,
                "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]]
            } );
		});



  </script>
@endsection




