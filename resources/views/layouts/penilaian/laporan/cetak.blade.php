<html>
    <head>
        <title></title>
    </head>
    <style>
        table, td, th {  
        border: 1px solid #ddd;
        text-align: left;
        }
        table {
        border-collapse: collapse;
        width: 100%;
        }
        th, td {
        padding: 3px;
        font-family: "Gill Sans", sans-serif;
        }
        body {
        -webkit-print-color-adjust: exact !important;
        }
    </style>
    

    <body>
    {{-- tabel header --}}
    <table>
        <tr>
            <td>Periode Penilaian</td>
            <td colspan="2"></td>
            <td colspan="2">Nilai</td>
            <td colspan="2">Penilaian / Daftar Nilai</td>
        <tr>
        <tr>
            <td>Nama Karyawan</td>
            <td colspan="2">{{ $pegawai->nama }}</td>
            <td>Nilai Tahun lalu</td>
            <td>{2021}</td>
            <td>Sangat Baik</td>
            <td>100</td>
        <tr>
        <tr>
            <td>Bagian-NIP</td>
            <td>{{ $pegawai->getdivisi->namadivisi }}</td>
            <td>{214310255}</td>
            <td>Nilai Sekarang</td>
            <td>{90}</td>
            <td>Baik</td>
            <td>90</td>
        </tr>
        <tr>
            <td>Tanggal Mulai Kerja</td>
            <td colspan="2">{8 Mei 2022}</td>
            <td></td>
            <td></td>
            <td>Cukup</td>
            <td>80</td>
        </tr>
        <tr>
            <td>Tanggal Cetak Penilaian</td>
            <td colspan="2">{8 Mei 2022}</td>
            <td></td>
            <td></td>
            <td>Tidak Baik</td>
            <td>70</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"></td>
            <td></td>
            <td></td>
            <td>Sangat Tidak Baik</td>
            <td>60</td>
        </tr>
    </table>

    {{-- tabel kinerja umum --}}
    <table>
        <tr>
            <th style="background-color:grey;color:white">Faktor Kompetensi</th>
            <th style="background-color:grey;color:white">Poin</th>
            <th style="background-color:grey;color:white">Target</th>
            <th style="background-color:grey;color:white">Rata-rata</th>
        </tr>
        <tr>
            <td colspan="4"><b>1. Kepribadian dan Prilaku</b></td>
        <tr>
        @foreach($mapumum as $key => $value)
        <tr>
            <td>{{ $value->nama->nama }}</td>
            <td>{{ $value->subtotal }} Poin</td>
            <td>100</td>
            <td>{{ round($value->subtotal/$jumlahpenilai,2) }}</td>
        <tr>    
        @endforeach
        {{-- grade --}}
        
        {{-- table kinerja perdivisi --}} 
        <tr>
            <td colspan="4"><b>2. Prestasi dan Hasil Kerja</b></td>
        <tr>
        @foreach($mapkinerja as $key => $value)
        <tr>
            <td>{{ $value->nama->nama }}</td>
            <td>{{ $value->subtotal }} Poin</td>
            <td>100</td>
            <td>{{ round($value->subtotal/$jumlahpenilai,2) }}</td>
        <tr>    
        @endforeach
        <tr>
            <td colspan="3"  style="background-color:grey;color:white">Grade</td>
            <td>{{ $grade }}</td>
        </tr>

    </table>
    {{-- tanda tangan  --}}
    <table style="margin-top:40px;border:0px !important">
        <tr style="border:0px !important">
            <td style="text-align:center;border:0px !important">Kasubag Adum</td>
            <td style="text-align:center;border:0px !important">Kepala Balai</td>
            <td style="text-align:center;border:0px !important">Atasan Langsung</td>
        </tr>
        <tr>
            <td style="text-align:center;border:0px !important">{ttd kasubag}</td>
            <td style="text-align:center;border:0px !important">{ttd kepalabalai}</td>
            <td style="text-align:center;border:0px !important">{ttd atasan langsung}</td>
        </tr>
        <tr>
            <td style="text-align:center;border:0px !important">{R Wisnu D H}</td>
            <td style="text-align:center;border:0px !important">{Khairul Bahri}</td>
            <td style="text-align:center;border:0px !important">{Mustain}</td>
        </tr>
    </table>
    
    
    
    <body>
</html>