@extends('layouts.app')

@section('content')
<div class="container">


    {{-- header atas --}}
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#305399;color:white">Jumlah yang sudah menilai anda</div>
                    <div class="panel-body">
                        <h2 style="font-weight:600;font-family:Roboto;text-align:center">
                        @if(isset($jumlahpenilai))
                        {{ $jumlahpenilai }} Orang
                        @else
                        0
                        @endif
                        </h2>
                    </div>
        </div>
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
        </div>
    </div>
    {{-- end header atas --}}
    <div class="row">
        <div class="col-md-12">
			 <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#305399;color:white">Cetak Laporan</div>
                
                <div class="panel-body">
                    {{-- div akan dibagi menjadi 3 --}}
                    <form action="" method="">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-4">
                        <label>Masukkan nama pegawai yang akan dicetak</label>
                        </div>

                        <div class="col-md-4">
                        <select class="js-example-basic-multiple" name="states[]" multiple="multiple">
                            @foreach($users as $value)
                                <option value="{{ $value->id }}">{{ $value->nama }}</option>    
                            @endforeach
                        </select>
                        
                        </div>

                        <div class="col-md-4">
                        <button type="submit" class="btn btn-md btn-info">cetak</button>
                        </div>
                    </div>
                     </form>

                </div>
             </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	  	$(document).ready(function() {
    		$('.js-example-basic-multiple').select2();
		});
  </script>
@endsection