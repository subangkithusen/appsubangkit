@extends('layouts.app') @section('content') <div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Form Transaksi Pegawai SWAB/PCR <a href="{{URL::to('/sjp')}}/create" class="btn btn-sm btn-info" style="float: right">Refresh</a></div>
        <div class="panel-body">
          <!-- halaman pcr/swab -->
          <form class="form-horizontal" action="#" id="kotak">
            <div class="form-group">
              <label class="control-label col-sm-2" for="email">Penjamin:</label>
              <div class="col-sm-10">
                <!-- select option -->
                <select id="js-example-basic-single2" name="cars">
                  <option value="volvo">PPK</option>
                  <option value="saab">Kepala Balai</option>
                </select>
                <!-- end select option -->
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd"></label>
              <div class="col-sm-10">
                <label>Masukkan nama Pegawai yang akan di SWAB or PCR :</label><br>
                    <select id="selectBox" class="js-example-basic-single">
                      <option value="khairul">Khairul</option>
                      <option value="subangkit">Subangkit</option>
                      <option value="ihsan">Ihsan</option>
                    </select>
                    <button id="add" class="add btn-success btn-sm">+ Add</button> <a href="#" class="btn btn-default btn-sm">+ Daftar Nama baru</a>
                    <div class="isi" style="padding-top: 5px"></div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Untuk Melakukkan : </label>
              <div class="col-sm-10">
                <!-- open -->
                <select id="js-example-basic-single3" name="cars">
                  <option value="volvo">PCR test</option>
                  <option value="saab">SWAB Antigen</option>
                </select>
                <!-- close -->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Tempat Pelaksana : </label>
              <div class="col-sm-10">
                <!-- open -->
                <select id="js-example-basic-single4" name="cars">
                  <option value="volvo">NH </option>
                  <option value="saab">BBLK</option>
                </select>
                <!-- close -->
                <a href="#">+ Penyedia </a>

              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Pembayaran : </label>
              <div class="col-sm-10">
                <label>dibebankan atau ditagihkan kepada DIPA BPFK Surabaya Tahun 2021</label>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Alamat Penagihan : </label>
              <div class="col-sm-10">
                <label>Kantor BPFK Surabaya<br>Jalan Karang Menjangan no.22 Surabaya</label>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success btn-md">Buat SJP</button>
              </div>
            </div>
          </form>
          <!-- end halaman pcr/swab -->
        </div>
      </div>
    </div>
  </div>
</div> @endsection

@section('js')
<script type="text/javascript">
  
  $(document).ready(function(){
    $('.js-example-basic-single').select2();

     $('#js-example-basic-single2').select2();
     $('#js-example-basic-single3').select2();
     $('#js-example-basic-single4').select2();


    $('#add').click(function(event){
    var tambahkotak = $('.isi');
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;

    console.log(selectedValue);
    // alert(selectedValue)

    event.preventDefault(); 
    var tabel ='';
    $('<div><label><input name="pegawai[]" value="'+selectedValue+'"/></label><button id="remove" class="btn btn-sm btn-danger" style="padding-bottom:3px;margin-bottom:3px">X</button></div>').appendTo(tambahkotak);
  });



  //   $('<div id="box"><textarea id="name" class="name"/></textarea><button id="remove">Hapus</button></div>').appendTo(tambahkotak);
  // });
  
  $('body').on('click','#remove',function(){  
    $(this).parent('div').remove(); 
  });   


  });


</script>

@endsection