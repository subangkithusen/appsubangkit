@extends('layouts.app') @section('content') <div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-warning">
        <div class="panel-heading"><b>Form Penugasan / PLH </b><a href="{{URL::to('/spt')}}/create" class="btn btn-sm btn-info" style="float: right">Refresh</a></div>
        <div class="panel-body">
          <!-- halaman pcr/swab -->
          <form class="form-horizontal" action="#" id="kotak">
            <div class="form-group">
              <label class="control-label col-sm-2" for="email">Nomor surat:</label>
              <div class="col-sm-10">
                <!-- select option -->  
                <input type="text" name="kp" value="KP.03.04" width="100px">
                <input type="text" name="kp" value="XLX.I">
                <input type="text" name="kp" value="">
                <input type="text" name="kp" value="2021">
                <!-- end select option -->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="exampleFormControlTextarea1">Perihal</label>
              <div class="col-sm-6">
              
              <textarea class="form-control " id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="email">Tanggal:</label>
              <div class="col-sm-2">
                <!-- select option -->  
                <input class="date form-control"  type="text" id="datepicker" name="date">
               
                <!-- end select option -->
              </div>


              <div class="col-sm-2">
                <!-- select option -->  
                <input class="date form-control"  type="text" id="datepicker2" name="date">
               
                
                <!-- end select option -->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd"></label>
              <div class="col-sm-6">
                <label>Masukkan Pihak ke-1(Pertama) :</label><br>
                    <select id="selectBox" class="form-control form-control-sm">
                      <option value="khairul">Khairul</option>
                      <option value="subangkit">Subangkit</option>
                      <option value="ihsan">Ihsan</option>
                    </select>
                    <button id="add" class="add btn-default btn-sm">cek</button> 
                    <div class="isi" style="padding-top: 5px"></div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Nip : </label>
              <div class="col-sm-10">
                <!-- open -->
                <label>{123}</label>
                <!-- close -->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Jabatan : </label>
              <div class="col-sm-10">
                <!-- open -->
                <label>{Kepala Balai}</label>
                <!-- close -->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Instansi : </label>
              <div class="col-sm-10">
                <!-- open -->
                <label>Balai Pengamanan Fasilitas Kesehatan Surabaya</label>
                <!-- close -->
              </div>
            </div>
            <hr>

            <!-- pihak 2 -->
            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd"></label>
              <div class="col-sm-6">
                <label class="">Masukkan Pihak ke-2(Kedua) :</label><br>
                    <select id="selectBox2" class="form-control form-control-sm">
                      <option value="khairul">Khairul</option>
                      <option value="subangkit">Subangkit</option>
                      <option value="ihsan">Ihsan</option>
                    </select>
                    <button id="add" class="add btn-default btn-sm">+ cek</button> 
                    <div class="isi" style="padding-top: 5px"></div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Nip : </label>
              <div class="col-sm-10">
                <!-- open -->
                <label>{123}</label>
                <!-- close -->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Jabatan : </label>
              <div class="col-sm-10">
                <!-- open -->
                <label>{Kepala Balai}</label>
                <!-- close -->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Instansi : </label>
              <div class="col-sm-10">
                <!-- open -->
                <label>Balai Pengamanan Fasilitas Kesehatan Surabaya</label>
                <!-- close -->
              </div>
            </div>


            <!-- tembusan -->
            <hr>
            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd"></label>
              <div class="col-sm-4">
                <label>Tembusan :</label><br>
                    <select id="selectBox" class="form-control form-control-sm">
                      <option value="khairul">Khairul</option>
                      <option value="subangkit">Subangkit</option>
                      <option value="ihsan">Ihsan</option>
                    </select>
                    <button id="add" class="add btn-success btn-sm">+ add</button> 
                    <div class="isi" style="padding-top: 5px"></div>
              </div>
            </div>

            <!-- end tembusan -->

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success btn-md" style="float:right">Buat PLH</button>
              </div>
            </div>
          </form>
          <!-- end halaman pcr/swab -->
        </div>
      </div>
    </div>
  </div>
</div> @endsection

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">

  
  $(document).ready(function(){

    $('#datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
         });

    $('#datepicker2').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
         });


    
     


    $('#add').click(function(event){
    var tambahkotak = $('.isi');
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;

    console.log(selectedValue);
    // alert(selectedValue)

    $.ajax({
      

    });


    event.preventDefault(); 
    var tabel ='';
    $('<div><label><input name="pegawai[]" value="'+selectedValue+'"/></label><button id="remove" class="btn btn-sm btn-danger" style="padding-bottom:3px;margin-bottom:3px">X</button></div>').appendTo(tambahkotak);
  });



  //   $('<div id="box"><textarea id="name" class="name"/></textarea><button id="remove">Hapus</button></div>').appendTo(tambahkotak);
  // });
  
  $('body').on('click','#remove',function(){  
    $(this).parent('div').remove(); 
  });   


  });


</script>

@endsection