@extends('layouts.app') @section('content') <div class="container">
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<!-- script untuk css -->
<style type="text/css">
  .select2 {
  width:100%!important;
  }


</style>
@endpush
<form action="{{route('surattugas.store')}}" method="POST">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">FORM SURAT TUGAS</div>
        <div class="panel-body">
            <!-- form -->
            <div class="row">
              
                {{csrf_field()}}
                <div class="col-md-4">
                  <!-- konten kiri -->
                  

                  
                    <label for="email">NOMOR:</label><br>
                    <select name="pertama" id="">
                    <option value="yk">YK</option>
                    <option value="ku">KU</option>
                    <option value="ym">YM</option>
                  </select>

                  <select name="kedua" id="">
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                  </select>

                  <select name="ketiga" id="">
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                  </select>
                  /
                  <select name="keempat" id="">
                    <option value="XLIX.1">XLIX.1</option>
                    <option value="XLIX.2">XLIX.2</option>
                    <option value="XLIX.3">XLIX.3</option>
                  </select>
                  / 
                  <input type="number" name="nomer" placeholder="ex:3333" style="width:100px;background-color:#00b894;border:1px solid;color:white;border-radius:5px"/>
                  <br><span style="color:#2d3436;font-size:8pt">note : silahkan dikosongi jika belum tahu nomernya</span>
                  
                  <div class="form-group">
                    <label for="email">ACARA /PERIHAL:</label>
                    <input type="text" class="form-control" id="email" name="perihal">
                  </div>
                  <label for="">KOORDINATOR <input type="checkbox" id="koor" name="koor" value="koor"></label>
                  <select class="form-control form-control-sm" id="koordinator" name="koordinator">
                        <option value="0">-pilih-</option>
                        @foreach($pegawais as $p)
                        <option value="{{$p->id}}" >{{$p->nama}}</option>
                        @endforeach
                  </select>

                  <div class="form-group">
                    <label for="email">TANGGAL MULAI DL:</label>
                      <div class='input-group date' id='datetimepicker1'>
                      <input type='text' class="form-control" name="tglmulai" />
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                      </div>
                  </div>

                  <div class="form-group">
                    <label for="email">TANGGAL SELESAI:</label>
                    <div class='input-group date' id='datetimepicker2'>
                      <input type='text' class="form-control" name="tglselesai"/>
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                      </div>
                  </div>

                  <!-- end kiri -->
                </div>

                <div class="col-md-8">
                    <!-- Petugas / Pegawai -->
                    <div class="row">
                      <div class="col-md-4">
                      <label for="exampleFormControlTextarea1">PEGAWAI</label>
                      <select class="form-control form-control-sm" id="pegawai">
                        <option value="-">-pilih-</option>
                        @foreach($pegawais as $p)
                        <option value="{{$p->id}}" >{{$p->nama}}</option>
                        @endforeach
                      </select>

                      </div>
                      <div class="col-md-2">
                      <label for="exampleFormControlTextarea1" style="color:white">.</label>
                        <button class="form-control btn btn-sm btn-success" id="pilih" style="background-color:#00b894;border-color:#00b894">Pilih</button>
                      </div>

                      <div class="col-md-2">
                      <label for="exampleFormControlTextarea1" style="color:white">.</label>
                      <button class="form-control btn btn-sm btn-default" id="bersih">bersihkan</button>
                       
                      </div>
                    </div>
                    <br>

                    <!-- end pegawai -->
                    
                   <!-- konten kanan -->

                   <!-- tabel pegawai detail  -->
                   <div class="row">
                     <div class="col-md-12">
                      <table width="100%">
                        <thead style="background-color:#00b894;color:white;margin-top:10px;margin-bottom:10px">
                          <th style="padding: 5px;px">NAMA</th>
                          <th>NIP/NIK</th>
                          <th>PANGKAT/GOL</th>
                          <th>JABATAN</th>
                        </thead>
                        <tbody id="detail">
                          
                          
                        </tbody>
                        
                      </table>
                     </div>

                   </div>

                   <!-- end detail tabel pegawai  -->
                   <br>

                   <div class="form-group">
                        <label for="exampleFormControlTextarea1">KONTEN</label>
                        <textarea class="form-control" id="konten" rows="3" name="editor1"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">KEPALA BALAI / PLH</label>
                        <select name="persetujuan" class="form-control form-control-sm" id="persetujuan">
                          @foreach($pegawais as $ps)
                          <option value="{{$ps->id}}">{{$ps->nama}}</option>
                          @endforeach
                        </select>
                      </div>

                    <!-- radiobutton  -->
                    <div class="row">

                     
                        <!-- <div class="col-md-2">
                        <input type="radio" id="css" name="ckr" value="TU"> TU

                        </div>

                        <div class="col-md-2">
                        <input type="radio" id="css" name="ckr" value="KAK"> KAK

                        </div> -->

                    </div>
                    

                    <!-- end radiobutton  -->
                    

                  

                  
                   <!-- en kanan -->

                </div>

            </div>

            <div class="row">
               
                    <div class="col-md-4">
                      
                    </div>

                    <div class="col-md-4">
                       <!-- <input type="submit" value="kirim"> -->
                        <button type="submit" class="btn btn-md btn-success" style="background-color:#00b894;border-color:#00b894">SIMPAN</button>
                        <button class="btn btn-md btn-warning" style="background-color:#fdcb6e;border-color:#fdcb6e">BERSIHKAN</button>
                    </div>

                    <div class="col-md-4">
                       
                    </div>  
                    
            </div>
            <!-- end form -->
        </div>
      </div>
    </div>
  </div>
  </form>
</div> @endsection

@section('js')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<!-- <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script> -->
<script type="text/javascript">
  
  $(document).ready(function(){

    // check uncheck 

    $("#koor").change(function() {
    if(this.checked) {
      // $('#koordinator').attr('disabled','disabled');
      $("#koordinator").removeAttr('disabled');
        //I am checked
        // alert("checkd")
    }else{
      $('#koordinator').attr('disabled','disabled');
        //I'm not checked
        // alert("not chacked")
    }
  });



    $('#datetimepicker1').datetimepicker({
      format:'DD/MM/YYYY'
    });

    $('#datetimepicker2').datetimepicker({
      format:'DD/MM/YYYY'
    });
    var htmltu;
    var htmlkak;
    var a = templatetu();
    $('#konten').val(a);

    $('#bersih').click(function(e){
      e.preventDefault();
      $('#detail').html("");
    });
    


    // radio buttin untuk ck editor
    $('input:radio[name=ckr]').change(function() {
        if (this.value == 'TU') {
            alert("TU");
            var a = templatetu();
            $('#konten').val('testdata');
            console.log(a);
        }
        else if (this.value == 'KAK') {
            alert("KAK");
        }
    }); 
    // end radio button
    $('#koordinator').attr('disabled','disabled');
    

    // onclick 
    $('#pilih').click(function(e){
      e.preventDefault();
      var pilihan = $('#pegawai').find(":selected").val();
      var test = "/jsonpegawi/"+pilihan;
      // console.log(test);
      $.ajax({
        url: "/jsonpegawi/"+pilihan,
        type: 'GET',
        success(data){
          console.log(data)
          var html;
                         html = '<tr style="border-bottom:solid 1px #00b894">;'
                            
                            html += '<td><input type="hidden" name="petugas[]" value="'+data.id+'">'+data.nama+'</td>;'
                            html += '<td>'+data.nik+'</td>;'
                            html += '<td>'+data.pangkat+'</td>;'
                            html += '<td>'+data.jabatan+'</td>;'
                          html += '</tr>;'
          // append
          $('#detail').append(html);
        }
      });
      
    });
    
    CKEDITOR.replace( 'editor1' );
    
    
    // $('#golongan').select2();

    //  $('#jabatan').select2();
    //  $('#divisi').select2();


    $('#add').click(function(event){
    var tambahkotak = $('.isi');
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;

    console.log(selectedValue);
    // alert(selectedValue)

    event.preventDefault(); 
    var tabel ='';
    $('<div><label><input name="pegawai[]" value="'+selectedValue+'"/></label><button id="remove" class="btn btn-sm btn-danger" style="padding-bottom:3px;margin-bottom:3px">X</button></div>').appendTo(tambahkotak);
  });



  //   $('<div id="box"><textarea id="name" class="name"/></textarea><button id="remove">Hapus</button></div>').appendTo(tambahkotak);
  // });
  
  $('body').on('click','#remove',function(){  
    $(this).parent('div').remove(); 
  });   


  });


  function templatetu(){
    // htmltu 
    htmltu = '<p>Untuk melaksanakan kegiatan yang akan dilaksanakan dengan rincian sebagai berikut :</p><p>Acara&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Seleksi CASN Kementerian Kesehatan RI Pada Kantor Regional II BKN&nbsp;Surabaya Jawa Timur&nbsp;&nbsp;&nbsp;&nbsp;<br>Hari / Tanggal&nbsp;&nbsp;&nbsp;&nbsp;: Senin - Kamis, 11 - 14 Oktober 2021&nbsp;&nbsp;&nbsp;&nbsp;<br>Tempat&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : Gedung Jatim Expo&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jl. Ahmad Yani No. 99 Surabaya&nbsp;&nbsp;</p>'
    return htmltu;
  }

  function templatekak(){
  }


</script>

@endsection