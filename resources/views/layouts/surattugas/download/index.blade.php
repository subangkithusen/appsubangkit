<html>
    <head>
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
        <title>
            download surat tugas 
        </title>

        <style>
            /* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 10pt;
    line-height:20px;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
    margin-top:5px;
    margin-bottom:5px;
}
th, td {
  padding: 5px;
  text-align: left;
  border:1px solid black;
}
.header{
    margin-top:120px;
    margin-bottom:20px;
}
.container{
    margin-left:20px;
    margin-right:20px;
    /* position: relative; */
}
.containersign{
    margin-top:60px;
    width:300px;
    /* height:100px; */
    /* background:blue; */
    /* border:solid 1px black; */
    float:right;
}

.containersign #signheader{
    width:290px;
    /* background:yellow; */
    margin-bottom:80px;
}
.containersign #signfooter{
    width:290px;
    /* background:green; */
}
.footer{
    width: 100%;
    /* height: 51px; */
    /* padding-left: 10px; */
    /* margin-top:80%; */
    line-height: 50px;
    /* background: #333; */
    color: #fff;
    position: absolute;
    bottom: 0px;
}
        </style>


    </head>
    <body>

    <div class="container">
        <div class="header">
            <center>
                <h3><b>SURAT TUGAS</b></h3>
                {{strtoupper($surat['aliassurat'])}}/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/2021
            </center>
        </div>

        <div class="body">
            <p>
            Kepala Balai Pengamanan Fasilitas Kesehatan Surabaya, dengan ini menugaskan kepada :</p>

            <span>
                <table>
                    <tr >
                        <td style="text-align:center">NO</td>
                        <td style="text-align:center">NAMA</td>
                        <td style="text-align:center">NIP</td>
                        <td style="text-align:center">PANGKAT / GOLONGAN</td>
                        <td style="text-align:center">JABATAN</td>
                    </tr>
                    <tbody>
                        <?php $a=1 ?>
                        @foreach($surat->detailsurat as $u)
                        <tr>
                            <td>{{$a++}}</td>
                            <td>
                              
                            {{$u->pegawai['nama']}}
                            </td>
                            <td>{{$u->pegawai['nip']}}</td>
                            <td>{{$u->pegawai->golongan['namapangkat']}} / {{$u->pegawai->golongan['golongan']}}</td>
                            <td>{{$u->pegawai->jabatan['namajabatan']}}</td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </span>

            <div>
            {!!$surat['konten']!!}
           
            Demikian surat tugas ini dibuat untuk dilaksanakan sebaik – baiknya, atas perhatian dan kerjasamanya disampaikan terimakasih.
            
            </div>
            <!-- container sign -->
            <div class="containersign">
                
                <?php
                $data = \App\PegawaiModel::find($surat['sign_id']);
                setlocale(LC_TIME, 'id_ID');
                \Carbon\Carbon::setLocale('id');
                $today = \Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
                $tanggal = ($surat['created_at'])->formatLocalized("%d %B %Y");
                ?>
               
                <div id="signheader">
                {{$tanggal}}
                <br>

                <!-- if jika kepala -->
                
                @if($data['id'] == 1)
                Kepala Balai,<br>
                @else
                an.Kepala Balai,<br>
                @endif
                <!-- jika kasub -->
                <!-- end if  -->
               	{{$data->jabatan['namajabatan']}}
                </div>
                <div id="signfooter">
                   

                <b>{{$data['nama']}}</b><br>
                NIP. {{$data['nip']}}
                </div>
            </div>
            <!-- end container sign -->

        </div>

        <div class="footer" style="margin-bottom:10px">
            {!!$qr!!}
            <!-- <img src="data:image/png;base64, {!! base64_encode(SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(50)->generate('Generier einen QrCode!')) !!} "> -->
            
        </div>

    </div>

    </body>
    <script>
        
    </script>
</html>