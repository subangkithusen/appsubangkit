@extends('layouts.app')
@push('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
@endpush
@section('content')
<div class="container">

<!-- modal open -->

  <!-- Trigger the modal with a button -->
  <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form Beri Nomor</h4>
        </div>
        <div class="modal-body">
          <span id="info">
          

          </span>
          <form action="#" method="post" id="formnomor">
            {{csrf_field()}}
            <span id="idsurat"></span>
          
          <div class="input-group">
      <input type="number" class="form-control" placeholder="beri nomor" name="nomor" id="nomor" required>
      <div class="input-group-btn">
        <button class="btn btn-default" type="submit" id="berinommor"><i class="glyphicon glyphicon-floppy-disk"></i> Berinomor</button>
      </div>
    </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default tutup" data-dismiss="modal" >Close</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- modal close -->



<!-- cetak all barcode fileter -->
<!-- <div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <p>Filter cetak </p>

      </div>

      <div class="panel-body">
        <form action="#">
        <div class="form-group">
          <label for="sel1">Pilih Devisi:</label>
          <select class="form-control" id="sel1">
            <option>All</option>
            <option>TU</option>
          </select>
          <button class="btn btn-md btn-primary" style="margin-top:5px"> CARI </button>
        </div>
          
        </form>

      </div>

    </div>

  </div>

</div> -->
<!-- end cetak barcode -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading"><a href="{{route('surattugas.create')}}"><span class="btn btn-sm btn-success">+ Buat Baru</span></a></div>
  
                <div class="panel-body">
                    <p>DATA SURAT KELUAR DL</p>
                    <table id="example" class="table" style="width:100%">
  <thead >
    <tr >
      <th>NOMER SURAT</th>
      <th>ACARA / KEGIATAN </th>
      <th>KOORDINATOR</th>
      <th>PETUGAS </th>
      <th>TANGGAl BERANGKAT</th>
      <th>TANGGAL SELESAI</th>
      
      <th></th>
    </tr>
  </thead>

  
  <tbody>
   
  @foreach($datasurat as $dt)
  @if(empty($dt->nomorsurat))
    <tr style='background-color:#fab1a0;color:white'>
      <td>
        @if(empty($dt->nomorsurat))
          <input type="hidden" name="idsurat" id="idsurat" value="{{$dt->id}}">
          <!-- id="berinomor" -->
          <a href="#" class="btn btn-sm btn-default style=background-color:#b2bec3"  data-toggle="modal" onClick="idSurat({{$dt->id}})">Beri Nomer</a>
        @else
          {{$dt->nomorsurat}}
        @endif
       
      </td>
      <td>{{$dt->acara}}</td>
      <td>{{$dt->koordinator['nama']}}</td>
      <!-- <td></td> -->
      <td>
        <!-- {{$dt->detailsurat}} -->
        @foreach($dt->detailsurat as $pg)
          {{$pg->pegawai['nama']}},<br>
        @endforeach


      </td>
      <td>{{$dt->tanggal_mulai}}</td>
      <td>{{$dt->tanggal_selesai}}</td>
      <td><a href="{{URL::to('/surattugas')}}/{{$dt->id}}" class="btn btn-xs btn-default" target="blank" style="background-color:#fdcb6e;border-color:#fdcb6e">UBAH</a> <a href="{{URL::to('/cetakpdf')}}/{{$dt->id}}" class="btn btn-xs btn-default" target="_blank">PDF</a><a href="" class="btn btn-xs btn-default">HAPUS</a></td>
    </tr>

    @else

    <tr>
      <td>
        @if(empty($dt->nomorsurat))
          <a href="#" class="btn btn-xs btn-default style=background-color:##b2bec3" id="berinomor" data-toggle="modal">Beri Nomer</a>
        @else
          {{$dt->nomorsurat}}
        @endif
       
      </td>
      <td>{{$dt->acara}}</td>
      <td>{{$dt->koordinator['nama']}}</td>
      <!-- <td></td> -->
      <td>
        <!-- {{$dt->detailsurat}} -->
        @foreach($dt->detailsurat as $pg)
          {{$pg->pegawai['nama']}},<br>
        @endforeach


      </td>
      <td>{{$dt->tanggal_mulai}}</td>
      <td>{{$dt->tanggal_selesai}}</td>
      <td><a href="{{URL::to('/surattugas')}}/{{$dt->id}}" class="btn btn-xs btn-default" target="blank" style="background-color:#fdcb6e;border-color:#fdcb6e"><i class="fa fa-edit" ></i>UBAH</a> <a href="{{URL::to('/cetakpdf')}}/{{$dt->id}}" class="btn btn-xs btn-default" target="_blank">PDF</a></td>
    </tr>


    @endif


    @endforeach
  
  
    
  </tbody>
</table>



                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('js')

<script type="text/javascript">
  $(document).ready( function () {
    $("#example").DataTable();
    // tutup / close modal nomor
    $('.tutup').click(function(e){
      location.reload();
    });
    // end modal nomor

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    // $('#example').DataTable();

    $('#berinomor').click(function(e){
      var idsurat = $("input[name=idsurat]").val();
      alert(idsurat);
      // $('#myModal').modal({backdrop: 'static', keyboard: false}) 
      // $('#myModal').modal('show');
     
      // panggil modal dialog
    })
    // alert("test");
      // $('#myTable').DataTable();
      // var a =  $('#myTable').DataTable();
      // console.log(a)
      // alert("test");

      $('#berinommor').click(function(e){
        e.preventDefault();
        $('#info').html('');
        var info;
        // alert("test")  
          $.ajax({
          type: "POST",
          url: "{{URL::to('/berinomor')}}",
          data:  $('#formnomor').serialize(), 
          success: function( data ) {
              console.log(data)
              if(data.status_code == 1){
                // info = '<p> NOMOR SUDAH DIPAKAI !!</p>';
                info = '<div class="alert alert-danger" role="alert"> NOMOR SUDAH DIPAKAI !!</div>'; 
              }else{
                info = '<div class="alert alert-success" role="alert">PEMBERIAN NOMOR SUKSES </div>';
              }
              $('#info').append(info);
            }
          });
      });

      function reload(){
        location.reload();
      }
  } );

  function idSurat(id){
    $('idsurat').html('')
    var html = '<input type="hidden" name="idsurat" value="'+id+'"/>';
    $('#idsurat').append(html);
    $('#myModal').modal({backdrop: 'static', keyboard: false}) 
  }


  
  </script>

  
  


@endsection
