<html>
    <head>
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
        <title>
            Preview surat tugas 
        </title>

        <style>
            /* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 10pt;
    line-height:20px;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
    margin-top:5px;
    margin-bottom:5px;
}
th, td {
  padding: 5px;
  text-align: left;
  border:1px solid black;
}
.header{
    margin-top:120px;
    margin-bottom:20px;
}
.container{
    margin-left:20px;
    /* position: relative; */
}
.containersign{
    margin-top:60px;
    width:300px;
    /* height:100px; */
    /* background:blue; */
    /* border:solid 1px black; */
    float:right;
}

.containersign #signheader{
    width:290px;
    /* background:yellow; */
    margin-bottom:80px;
}
.containersign #signfooter{
    width:290px;
    /* background:green; */
}
.footer{
    width: 100%;
    /* height: 51px; */
    /* padding-left: 10px; */
    /* margin-top:80%; */
    line-height: 50px;
    /* background: #333; */
    color: #fff;
    position: absolute;
    bottom: 0px;
}
        </style>


    </head>
    <body>

    <div class="container">
        <div class="header">
            <center>
                <h3>SURAT TUGAS</h3>
                YK.01.03/XLIX.4 /2611 /2021
            </center>
        </div>

        <div class="body">
            <p>
            Kepala Balai Pengamanan Fasilitas Kesehatan Surabaya, dengan ini menugaskan kepada :</p>

            <span>
                <table>
                    <tr >
                        <td style="text-align:center">NO</td>
                        <td style="text-align:center">NAMA</td>
                        <td style="text-align:center">NIP</td>
                        <td style="text-align:center">PANGKAT / GOLONGAN</td>
                        <td style="text-align:center">JABATAN</td>
                    </tr>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>HERMAN ARIANTO, SE, M.Si</td>
                            <td>197603292006041001</td>
                            <td>Penata Tingkat I / IIId</td>
                            <td>Analis Kepegawaian Ahli Muda (JF)</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>HERMAN ARIANTO, SE, M.Si</td>
                            <td>197603292006041001</td>
                            <td>Penata Tingkat I / IIId</td>
                            <td>Analis Kepegawaian Ahli Muda (JF)</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>HERMAN ARIANTO, SE, M.Si</td>
                            <td>197603292006041001</td>
                            <td>Penata Tingkat I / IIId</td>
                            <td>Analis Kepegawaian Ahli Muda (JF)</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>HERMAN ARIANTO, SE, M.Si</td>
                            <td>197603292006041001</td>
                            <td>Penata Tingkat I / IIId</td>
                            <td>Analis Kepegawaian Ahli Muda (JF)</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>HERMAN ARIANTO, SE, M.Si</td>
                            <td>197603292006041001</td>
                            <td>Penata Tingkat I / IIId</td>
                            <td>Analis Kepegawaian Ahli Muda (JF)</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>HERMAN ARIANTO, SE, M.Si</td>
                            <td>197603292006041001</td>
                            <td>Penata Tingkat I / IIId</td>
                            <td>Analis Kepegawaian Ahli Muda (JF)</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>HERMAN ARIANTO, SE, M.Si</td>
                            <td>197603292006041001</td>
                            <td>Penata Tingkat I / IIId</td>
                            <td>Analis Kepegawaian Ahli Muda (JF)</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>HERMAN ARIANTO, SE, M.Si</td>
                            <td>197603292006041001</td>
                            <td>Penata Tingkat I / IIId</td>
                            <td>Analis Kepegawaian Ahli Muda (JF)</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>HERMAN ARIANTO, SE, M.Si</td>
                            <td>197603292006041001</td>
                            <td>Penata Tingkat I / IIId</td>
                            <td>Analis Kepegawaian Ahli Muda (JF)</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>HERMAN ARIANTO, SE, M.Si</td>
                            <td>197603292006041001</td>
                            <td>Penata Tingkat I / IIId</td>
                            <td>Analis Kepegawaian Ahli Muda (JF)</td>
                        </tr>
                    </tbody>
                </table>
            </span>

            <div>
            {!!$datasurat['editor1']!!} <br>
            Demikian surat tugas ini dibuat untuk dilaksanakan sebaik – baiknya, atas perhatian dan kerjasamanya disampaikan terimakasih.
            
            </div>
            <!-- container sign -->
            <div class="containersign">
                <div id="signheader">
                8 Oktober 2021<br>	
                an.Kepala Balai,<br>	
                Ka.Sub.Bag Administrasi Umum	

                </div>
                <div id="signfooter">
                <b>RADEN WISNU DWI HARDYANTO, ST</b><br>
                NIP 197401111997031003
                </div>
            </div>
            <!-- end container sign -->

        </div>

        <div class="footer">
            {!!$qr!!}

        </div>

    </div>

       
           




            
           

    </body>
    <script>
        $(document).ready(function() {
            window.print();

            
        });
    </script>
</html>