@extends('layouts.app') @section('content') <div class="container">
    @push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
    <!-- script untuk css -->
    <style type="text/css">
        .select2 {
            width: 100% !important;
        }


</style>
@endpush
<form action="#" method="POST">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">FORM SURAT TUGAS <b>(UBAH DATA)</b></div>
        <div class="panel-body">
            <!-- form -->
            <div class="row">

                {{csrf_field()}}
                <div class="col-md-4">
                  <!-- konten kiri -->
                  <?php

                  $pertama = substr($surat->aliassurat,0,2);
                  $kedua = substr($surat->aliassurat,3,2);
                  $ketiga = substr($surat->aliassurat,6,2);
                  $keempat = substr($surat->aliassurat,9,6);

                  ?>


                                <label for="email">NOMOR:</label><br>
                                <select name="pertama" id="">
                                    @if($pertama == 'yk')
                                    <option value="yk" selected>YK</option>
                                    <option value="ku">KU</option>
                                    <option value="ym">YM</option>
                                    @elseif($pertama == 'ku')
                                    <option value="ku" selected>KU</option>
                                    <option value="yk">YK</option>
                                    <option value="ym">YM</option>

                                    @elseif($pertama == 'ym')
                                    <option value="ym" selected>YM</option>
                                    <option value="yk">YK</option>
                                    <option value="ku">KU</option>
                                    @else
                                    <option value="yk">YK</option>
                                    <option value="ku">KU</option>
                                    <option value="ym">YM</option>
                                    @endif
                                </select>

                                <select name="kedua" id="">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                </select>

                                <select name="ketiga" id="">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                </select>
                                /
                                <select name="keempat" id="">
                                    <option value="XLIX.1">XLIX.1</option>
                                    <option value="XLIX.2">XLIX.2</option>
                                    <option value="XLIX.3">XLIX.3</option>
                                </select>
                                /
                                <input type="number" name="nomer" placeholder="ex:3333" style="width:100px;background-color:#00b894;border:1px solid;color:white;border-radius:5px" />
                                <br><span style="color:#2d3436;font-size:8pt">note : silahkan dikosongi jika belum tahu nomernya</span>

                                <div class="form-group">
                                    <label for="email">ACARA /PERIHAL:</label>
                                    <input type="text" class="form-control" id="email" name="perihal" value="{{$surat->acara}}">
                                </div>
                                <label for="">KOORDINATOR <input type="checkbox" id="koor" name="koor" value="koor"></label>
                                <select class="form-control form-control-sm" id="koordinator" name="koordinator">
                                    <option value="0">-pilih-</option>
                                    @foreach($pegawais as $p)
                                    @if($surat->koordinator_id == $p->id)
                                    <option value="{{$p->id}}" selected>{{$p->nama}}</option>

                                    @else
                                    <option value="{{$p->id}}">{{$p->nama}}</option>

                                    @endif

                                    @endforeach
                                </select>

                                <div class="form-group">
                                    <label for="email">TANGGAL MULAI DL:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' class="form-control" name="tglmulai" value="{{$surat->tanggal_mulai}}" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email">TANGGAL SELESAI:</label>
                                    <div class='input-group date' id='datetimepicker2'>
                                        <input type='text' class="form-control" name="tglselesai" value="{{$surat->tanggal_selesai}}" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>

                                <!-- end kiri -->
                            </div>

                            <div class="col-md-8">
                                <!-- Petugas / Pegawai -->


                                <!-- end pegawai -->

                                <!-- konten kanan -->

                                <!-- tabel pegawai detail  -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-xs btn-success" style="margin-bottom:5px" id="tambahpetugas">+ tambah petugas / pegawai DL</button>
                                        <table width="100%">
                                            <thead style="background-color:#00b894;color:white;margin-top:10px;margin-bottom:10px">
                                                <th style="padding:5px;">NAMA</th>
                                                <th>NIP/NIK</th>
                                                <th>PANGKAT/GOL</th>
                                                <th>JABATAN</th>
                                                <th></th>
                                            </thead>
                                            <tbody id="detail">
                                                @foreach($surat->detailsurat as $dt)
                                                @if($dt->status_aktif == 1)
                                                <tr>
                                                    <td>{{$dt->pegawai->nama}}
                                                    </td>
                                                    <td>
                                                        {{$dt->pegawai->nip}}
                                                    </td>
                                                    <td>
                                                        {{$dt->pegawai->golongan->namapangkat}}/{{$dt->pegawai->golongan->golongan}}

                                                    </td>
                                                    <td>
                                                        {{$dt->pegawai->jabatan->namajabatan}}
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-xs btn-default" onClick="ubahDataPegawai({{$dt->id}},{{Request::segment(2)}})">U</button> <button class="btn btn-xs btn-danger" id="hapusPegawaidl" onClick="hapuspegawai({{$dt->id}},{{Request::segment(2)}})">x</button>
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach


                                            </tbody>

                                        </table>
                                    </div>

                                </div>

                                <!-- end detail tabel pegawai  -->
                                <br>

                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">KONTEN</label>
                                    <textarea class="form-control" id="konten" rows="3" name="editor1">

                        {!!$surat->konten!!}
                        </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">KEPALA BALAI / PLH</label>
                                    <select name="persetujuan" class="form-control form-control-sm" id="persetujuan">
                                        @foreach($pegawais as $ps)

                                        @if($surat->sign_id == $ps->id)
                                        <option value="{{$ps->id}}" selected>{{$ps->nama}}</option>

                                        @else
                                        <option value="{{$ps->id}}">{{$ps->nama}}</option>

                                        @endif

                                        @endforeach
                                    </select>
                                </div>

                                <!-- radiobutton  -->
                                <div class="row">


                                    <!-- <div class="col-md-2">
                        <input type="radio" id="css" name="ckr" value="TU"> TU

                        </div>

                        <div class="col-md-2">
                        <input type="radio" id="css" name="ckr" value="KAK"> KAK

                        </div> -->

                                </div>


                                <!-- end radiobutton  -->





                                <!-- en kanan -->

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-4">

                            </div>

                            <div class="col-md-4">
                                <!-- <input type="submit" value="kirim"> -->
                                <button type="submit" class="btn btn-md btn-success" style="background-color:#00b894;border-color:#00b894">SIMPAN</button>
                                <button class="btn btn-md btn-warning" style="background-color:#fdcb6e;border-color:#fdcb6e">BERSIHKAN</button>
                            </div>

                            <div class="col-md-4">

                            </div>

                        </div>
                        <!-- end form -->
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- modal -->
<div class="modal fade" id="myTambahPegawai" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Form Tambah Petugas DL</h4>
            </div>
            <div class="modal-body">
                <span id="info">


                </span>
                <form action="#" id="petugasform">
                    {{csrf_field()}}
                    <input type="hidden" name="surattugas_id" value="{{Request::segment(2)}}" id="surattugas_id">

                    <label for="">Nama Pegawai</label>
                    <select class='form-control form-control-sm' name="pegawaidl" id="pegawaidl">
                        @foreach($pegawais as $dt)
                        <option value="{{$dt->id}}">{{$dt->nama}}</option>
                        @endforeach
                    </select>
                    <button class="btn btn-sm btn-default" style="margin-top:5px;background-color:#00b894;border-color:#00b894;color:white" id="tambahPetugas">Tambah</button>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default tutup" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- endmodal -->


<!-- modal ubah pegawai -->
<div class="modal fade" id="pegawaiubah" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Form Ubah Petugas DL</h4>
            </div>
            <div class="modal-body">
                <span id="info">


                </span>
                <form action="#" id="ubahpetugasform">
                    {{csrf_field()}}
                    <input type="hidden" name="surattugas_id" value="{{Request::segment(2)}}" id="surattugas_id">

                    <label for="">Nama Pegawai yang ingin di ubah</label>
                    <input type="text" value="" name="pegawai_lama" class="form-group form-control" />
                    <select class='form-control form-control-sm' name="pegawaidl" id="pegawaidl">
                        @foreach($pegawais as $dt)
                        <option value="{{$dt->id}}">{{$dt->nama}}</option>
                        @endforeach
                    </select>
                    <button class="btn btn-sm btn-default" style="margin-top:5px;background-color:#00b894;border-color:#00b894;color:white" id="tambahPetugas">Ubah Data</button>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default tutup" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<!-- end ubah pegawai -->
@endsection

@section('js')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<!-- <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script> -->
<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#tambahPetugas").click(function(e) {
            e.preventDefault()
            var info;
            // alert("test");
            $.ajax({
                type: "POST"
                , data: $('#petugasform').serialize()
                , url: "{{URL::to('/suratdetail/post')}}"
                , success: function(data) {
                    console.log(data)
                    if (data.sukses) {
                        info = '<div class="alert alert-success" role="alert">DATA SUDAH MASUK DATABASE SYSTEM!</div>';
                        location.reload();
                    }
                    $("#info").append(info);
                }

            });
        });

        //   test dialog confirm


        //test

        // check uncheck 

        $("#koor").change(function() {
            if (this.checked) {
                // $('#koordinator').attr('disabled','disabled');
                $("#koordinator").removeAttr('disabled');
                //I am checked
                // alert("checkd")
            } else {
                $('#koordinator').attr('disabled', 'disabled');
                //I'm not checked
                // alert("not chacked")
            }
        });



        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD'
        });

        $('#datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        var htmltu;
        var htmlkak;
        // var a = templatetu();
        // $('#konten').val(a);

        $('#bersih').click(function(e) {
            e.preventDefault();
            $('#detail').html("");
        });



        // radio buttin untuk ck editor
        $('input:radio[name=ckr]').change(function() {
            if (this.value == 'TU') {
                alert("TU");
                var a = templatetu();
                $('#konten').val('testdata');
                console.log(a);
            } else if (this.value == 'KAK') {
                alert("KAK");
            }
        });
        // end radio button
        $('#koordinator').attr('disabled', 'disabled');

        CKEDITOR.replace('editor1');

        $('#tambahpetugas').click(function(e) {
            e.preventDefault();
            $('#myTambahPegawai').modal({
                backdrop: 'static'
                , keyboard: false
            })
            // alert("test");
        });


        // $('#golongan').select2();

        //  $('#jabatan').select2();
        //  $('#divisi').select2();


        $('#add').click(function(event) {
            var tambahkotak = $('.isi');
            var selectBox = document.getElementById("selectBox");
            var selectedValue = selectBox.options[selectBox.selectedIndex].value;

            console.log(selectedValue);
            // alert(selectedValue)

            event.preventDefault();
            var tabel = '';
            $('<div><label><input name="pegawai[]" value="' + selectedValue + '"/></label><button id="remove" class="btn btn-sm btn-danger" style="padding-bottom:3px;margin-bottom:3px">X</button></div>').appendTo(tambahkotak);
        });



        //   $('<div id="box"><textarea id="name" class="name"/></textarea><button id="remove">Hapus</button></div>').appendTo(tambahkotak);
        // });

        $('body').on('click', '#remove', function() {
            $(this).parent('div').remove();
        });


    });


    function templatetu() {
        // htmltu 
        htmltu = '<p>Untuk melaksanakan kegiatan yang akan dilaksanakan dengan rincian sebagai berikut :</p><p>Acara&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Seleksi CASN Kementerian Kesehatan RI Pada Kantor Regional II BKN&nbsp;Surabaya Jawa Timur&nbsp;&nbsp;&nbsp;&nbsp;<br>Hari / Tanggal&nbsp;&nbsp;&nbsp;&nbsp;: Senin - Kamis, 11 - 14 Oktober 2021&nbsp;&nbsp;&nbsp;&nbsp;<br>Tempat&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : Gedung Jatim Expo&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jl. Ahmad Yani No. 99 Surabaya&nbsp;&nbsp;</p>'
        return htmltu;
    }

    function templatekak() {}

    function ubahDataPegawai(id, pegawai_id) {
        event.preventDefault()
        $("#pegawaiubah").modal('show')
        // alert("test")

        var konfirmasi;
        konfirmasi = confirm("Apakah anda yakin mengubah data ini?");
        if (konfirmasi == true) {
            // ajax
            $.ajax({
                url: "{{URL::to('/suratdetail/')}}/" + id + '/' + pegawai_id
                , type: 'get'
                , dataType: 'JSON'
                , data: {
                    'id': id
                    , 'pegawai_id': pegawai_id,
                    // '_token': '{{ csrf_token() }}',
                }
                , success: function(data) {
                    console.log(data)
                    if (data.sukses == 200) {
                        // location.reload();
                    }
                }

            });
            // end ajax

        }
    }

    function hapuspegawai(id, surat_id) {
        var konfirmasi;
        event.preventDefault()
        var txt;
        konfirmasi = confirm("Apakah anda yakin menghapus ini?");
        if (konfirmasi == true) {
            $.ajax({
                url: "{{URL::to('/suratdetail/')}}/" + id + '/' + surat_id
                , type: 'DELETE'
                , dataType: 'JSON'
                , data: {
                    'id': id
                    , 'surat_id': surat_id
                    , '_token': '{{ csrf_token() }}'
                , }
                , success: function(data) {
                    console.log(data)
                    if (data.sukses == 200) {
                        location.reload();
                    }
                }

            });

        } else {
            alert("gak jadi hapus");
        }
        // alert(id)
    }

</script>

@endsection
