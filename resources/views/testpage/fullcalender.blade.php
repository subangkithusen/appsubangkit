<!DOCTYPE html>
<html>
<head>
    <title>Laravel Fullcalender </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
  
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>
  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
</head>
<body>
  
<div class="container">
    <h1>Laravel Calender</h1>
    <div id='calendar'></div>
</div>
   
<script>
$(document).ready(function () {
   
var SITEURL = "{{ url('/') }}";
  
$.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

  
var calendar = $('#calendar').fullCalendar({
                    editable: false,
                    //events: SITEURL + "/fullcalender",
                    events : [
                        //undangan parsing data dari controller
                        @foreach($data as $d)
                            {
                            title : '{{ $d->keterangan}}',
                            start : '{{ $d->start }}',
                            end : '{{ $d->end }}',
                            url: '{{ url('/undangan/lihat', $d->id) }}',
                            color: '#378006', 
                            },

                        @endforeach
                    ],
                    //eventColor: '#378006',
                    displayEventTime: false,
                    editable: false,
                });
 
});
 
function displayMessage(message) {
    toastr.success(message, 'Event');
} 
  
</script>
  
</body>
</html>