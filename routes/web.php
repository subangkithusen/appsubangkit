<?php
use App\Events\SendGlobalNotification;

Route::get('send-notif/{name}', function ($name) {
    event(new SendGlobalNotification($name));
    return "Event has been sent!";
});

Route::get('/testpusher', function () {
    return view('testpusher');
});


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/spt', 'SptController@index')->name('spt');
Route::get('/sjp', 'SptController@index')->name('sjp');


Route::get('/cetaksinglebarcode/{id}', 'BmnController@CetakRekapBRCD')->name('barcode.id'); //cetakbarcode single

Route::resource('/surattugas','SurattugasController');
Route::resource('/suratdetail','DetailsuratController');
Route::post('/berinomor','SurattugasController@berinomor');

//kompetensiumum
Route::resource('/kompetensiumum','Controllerkompetensiumum');
//kompetensikinerja

Route::resource('/kompetensikinerja','Controllerkompetensikinerja');

//penilaian
Route::get('/showpenilaian','Controllerpenialaiankinerja@showpenilaian')->name('showpenilaian');
Route::resource('/penilaiankerja','Controllerpenialaiankinerja');

//laporan
Route::get('/laporan','LaporankinerjaController@index')->name('laporankinerja');
Route::get('/cetak','LaporankinerjaController@cetak')->name('cetaklaporan');

//test cetak page-break
Route::resource('/testpage','testpageController');


// testcalender
Route::get('/fullcalender','EvenController@index');
// ajax calender
Route::post('/fullcalenderAjax','EvenController@ajax');

Route::resource('/sjp','SjpController');
Route::resource('/spt','SptController');
Route::resource('/bmn','BmnController');
Route::resource('/pegawai','PegawaiController');
Route::resource('/penyedia','PenyediaController');

Route::get('/jsonpegawi/{id}','SurattugasController@jsonpegawai');
Route::get('/cetakpdf/{id}','SurattugasController@cetakpdf');

Route::post('/suratdetail/post','DetailsuratController@tambahpertugas');
Route::delete('/suratdetail/{id}/{surat_id}','DetailsuratController@hapusPegawaiDL');
Route::get('/suratdetail/{id}/{pegawai_id}','DetailsuratController@ubahpegawaiJson');

